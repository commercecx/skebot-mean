function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["org-health-org-health-module"], {
  /***/
  "./src/app/pages/org-health/org-health-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/org-health/org-health-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: OrgHealthRoutingModule */

  /***/
  function srcAppPagesOrgHealthOrgHealthRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrgHealthRoutingModule", function () {
      return OrgHealthRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _org_health_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./org-health.component */
    "./src/app/pages/org-health/org-health.component.ts");
    /* harmony import */


    var _test_coverage_test_coverage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./test-coverage/test-coverage.component */
    "./src/app/pages/org-health/test-coverage/test-coverage.component.ts");

    var routes = [{
      path: '',
      component: _org_health_component__WEBPACK_IMPORTED_MODULE_2__["OrgHealthComponent"],
      children: [{
        path: 'testcoverage',
        component: _test_coverage_test_coverage_component__WEBPACK_IMPORTED_MODULE_3__["TestCoverageComponent"]
      }]
    }];

    var OrgHealthRoutingModule = function OrgHealthRoutingModule() {
      _classCallCheck(this, OrgHealthRoutingModule);
    };

    OrgHealthRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: OrgHealthRoutingModule
    });
    OrgHealthRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function OrgHealthRoutingModule_Factory(t) {
        return new (t || OrgHealthRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](OrgHealthRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](OrgHealthRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/org-health/org-health.component.ts":
  /*!**********************************************************!*\
    !*** ./src/app/pages/org-health/org-health.component.ts ***!
    \**********************************************************/

  /*! exports provided: OrgHealthComponent */

  /***/
  function srcAppPagesOrgHealthOrgHealthComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrgHealthComponent", function () {
      return OrgHealthComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var OrgHealthComponent = /*#__PURE__*/function () {
      function OrgHealthComponent() {
        _classCallCheck(this, OrgHealthComponent);
      }

      _createClass(OrgHealthComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return OrgHealthComponent;
    }();

    OrgHealthComponent.ɵfac = function OrgHealthComponent_Factory(t) {
      return new (t || OrgHealthComponent)();
    };

    OrgHealthComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: OrgHealthComponent,
      selectors: [["ngx-org-health"]],
      decls: 1,
      vars: 0,
      template: function OrgHealthComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
      encapsulation: 2
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](OrgHealthComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'ngx-org-health',
          template: "<router-outlet></router-outlet>"
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/org-health/org-health.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/org-health/org-health.module.ts ***!
    \*******************************************************/

  /*! exports provided: OrgHealthModule */

  /***/
  function srcAppPagesOrgHealthOrgHealthModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrgHealthModule", function () {
      return OrgHealthModule;
    });
    /* harmony import */


    var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/cdk/scrolling */
    "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
    /* harmony import */


    var ng2_search_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ng2-search-filter */
    "./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js");
    /* harmony import */


    var _utilities_utilities_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./../utilities/utilities.module */
    "./src/app/pages/utilities/utilities.module.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _org_health_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./org-health-routing.module */
    "./src/app/pages/org-health/org-health-routing.module.ts");
    /* harmony import */


    var _org_health_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./org-health.component */
    "./src/app/pages/org-health/org-health.component.ts");
    /* harmony import */


    var _test_coverage_test_coverage_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./test-coverage/test-coverage.component */
    "./src/app/pages/org-health/test-coverage/test-coverage.component.ts");
    /* harmony import */


    var _nebular_theme__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @nebular/theme */
    "./node_modules/@nebular/theme/__ivy_ngcc__/fesm2015/index.js");
    /* harmony import */


    var _theme_theme_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../@theme/theme.module */
    "./src/app/@theme/theme.module.ts");
    /* harmony import */


    var ngx_echarts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ngx-echarts */
    "./node_modules/ngx-echarts/__ivy_ngcc__/fesm2015/ngx-echarts.js"); // tslint:disable-next-line:max-line-length


    var OrgHealthModule = function OrgHealthModule() {
      _classCallCheck(this, OrgHealthModule);
    };

    OrgHealthModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({
      type: OrgHealthModule
    });
    OrgHealthModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({
      factory: function OrgHealthModule_Factory(t) {
        return new (t || OrgHealthModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _org_health_routing_module__WEBPACK_IMPORTED_MODULE_6__["OrgHealthRoutingModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbCardModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbButtonModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTreeGridModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbIconModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbInputModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTooltipModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbFormFieldModule"], _theme_theme_module__WEBPACK_IMPORTED_MODULE_10__["ThemeModule"], ngx_echarts__WEBPACK_IMPORTED_MODULE_11__["NgxEchartsModule"], _utilities_utilities_module__WEBPACK_IMPORTED_MODULE_2__["UtilitiesModule"], ng2_search_filter__WEBPACK_IMPORTED_MODULE_1__["Ng2SearchPipeModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_0__["ScrollingModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](OrgHealthModule, {
        declarations: [_org_health_component__WEBPACK_IMPORTED_MODULE_7__["OrgHealthComponent"], _test_coverage_test_coverage_component__WEBPACK_IMPORTED_MODULE_8__["TestCoverageComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _org_health_routing_module__WEBPACK_IMPORTED_MODULE_6__["OrgHealthRoutingModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbCardModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbButtonModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTreeGridModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbIconModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbInputModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTooltipModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbFormFieldModule"], _theme_theme_module__WEBPACK_IMPORTED_MODULE_10__["ThemeModule"], ngx_echarts__WEBPACK_IMPORTED_MODULE_11__["NgxEchartsModule"], _utilities_utilities_module__WEBPACK_IMPORTED_MODULE_2__["UtilitiesModule"], ng2_search_filter__WEBPACK_IMPORTED_MODULE_1__["Ng2SearchPipeModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_0__["ScrollingModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵsetClassMetadata"](OrgHealthModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"],
        args: [{
          declarations: [_org_health_component__WEBPACK_IMPORTED_MODULE_7__["OrgHealthComponent"], _test_coverage_test_coverage_component__WEBPACK_IMPORTED_MODULE_8__["TestCoverageComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _org_health_routing_module__WEBPACK_IMPORTED_MODULE_6__["OrgHealthRoutingModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbCardModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbButtonModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTreeGridModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbIconModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbInputModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbTooltipModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_9__["NbFormFieldModule"], _theme_theme_module__WEBPACK_IMPORTED_MODULE_10__["ThemeModule"], ngx_echarts__WEBPACK_IMPORTED_MODULE_11__["NgxEchartsModule"], _utilities_utilities_module__WEBPACK_IMPORTED_MODULE_2__["UtilitiesModule"], ng2_search_filter__WEBPACK_IMPORTED_MODULE_1__["Ng2SearchPipeModule"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_0__["ScrollingModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/org-health/test-coverage/test-coverage.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/pages/org-health/test-coverage/test-coverage.component.ts ***!
    \***************************************************************************/

  /*! exports provided: TestCoverageComponent */

  /***/
  function srcAppPagesOrgHealthTestCoverageTestCoverageComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestCoverageComponent", function () {
      return TestCoverageComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _core_interfaces_common_testCoverage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../@core/interfaces/common/testCoverage */
    "./src/app/@core/interfaces/common/testCoverage.ts");
    /* harmony import */


    var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/Subject */
    "./node_modules/rxjs-compat/_esm2015/Subject.js");
    /* harmony import */


    var _core_stores_dashboard_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../@core/stores/dashboard.store */
    "./src/app/@core/stores/dashboard.store.ts");
    /* harmony import */


    var _core_stores_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../@core/stores/actions */
    "./src/app/@core/stores/actions.ts");
    /* harmony import */


    var _nebular_theme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @nebular/theme */
    "./node_modules/@nebular/theme/__ivy_ngcc__/fesm2015/index.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _utilities_export_export_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../utilities/export/export.component */
    "./src/app/pages/utilities/export/export.component.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/cdk/scrolling */
    "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
    /* harmony import */


    var ng2_search_filter__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ng2-search-filter */
    "./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js"); // tslint:disable:indent


    function TestCoverageComponent_div_17_tr_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r103 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.ApexClassOrTriggerId);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.ApexClassOrTrigger);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.NumLinesCovered);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.NumLinesUncovered);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.TotalPercentage);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r103.LastModifiedBy);
      }
    }

    function TestCoverageComponent_div_17_tr_20_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " No Data Available ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function TestCoverageComponent_div_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "cdk-virtual-scroll-viewport", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "table", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "thead");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "th", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Id");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "th", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Name");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "th", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Lines Covered");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "th", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Lines Uncovered");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "th", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Percentage");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "th", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Last Modified By");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, TestCoverageComponent_div_17_tr_18_Template, 13, 6, "tr", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "filter");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestCoverageComponent_div_17_tr_20_Template, 3, 0, "tr", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r100 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("itemSize", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("cdkVirtualForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](19, 3, ctx_r100.testCoverage, ctx_r100.searchText));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r100.testCoverage.length == 0);
      }
    }

    var TestCoverageComponent = /*#__PURE__*/function () {
      function TestCoverageComponent(testCoverageService) {
        _classCallCheck(this, TestCoverageComponent);

        this.testCoverageService = testCoverageService;
        this.unsubscribe$ = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.testCoveragePercentage = 0;
        this.testCoverage = [];
        this.searchText = '';
        this.exportColumns = ['ApexClassOrTriggerId', 'ApexClassOrTrigger', 'NumLinesCovered', 'NumLinesUncovered', 'TotalPercentage', 'LastModifiedBy'];
      }

      _createClass(TestCoverageComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          _core_stores_dashboard_store__WEBPACK_IMPORTED_MODULE_3__["dashboardStore"].subscribe(function (state) {
            _this.testCoverage = state.testCoverage;
            _this.testCoveragePercentage = state.testCoveragePercentage;
          });

          _core_stores_dashboard_store__WEBPACK_IMPORTED_MODULE_3__["dashboardEventDispatcher"].next({
            type: _core_stores_actions__WEBPACK_IMPORTED_MODULE_4__["ActionTypes"].GET_TESTCOVERAGE
          });

          _core_stores_dashboard_store__WEBPACK_IMPORTED_MODULE_3__["dashboardEventDispatcher"].next({
            type: _core_stores_actions__WEBPACK_IMPORTED_MODULE_4__["ActionTypes"].GET_TESTCOVERAGEPERCENTAGE
          });
        }
      }]);

      return TestCoverageComponent;
    }();

    TestCoverageComponent.ɵfac = function TestCoverageComponent_Factory(t) {
      return new (t || TestCoverageComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_core_interfaces_common_testCoverage__WEBPACK_IMPORTED_MODULE_1__["TestCoverageData"]));
    };

    TestCoverageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TestCoverageComponent,
      selectors: [["ngx-test-coverage"]],
      decls: 18,
      vars: 18,
      consts: [[1, "header", "px-3"], [1, "mb-0", "header-content"], ["nbTooltip", "Overall Coverage", "nbTooltipStatus", "primary", 1, "ml-2", 2, "font-size", "11px", "padding", "4px 10px", "background-color", "#edf1f7", "border-radius", "5px"], [1, "header-content"], ["nbPrefix", "", "icon", "search-outline", "pack", "eva"], ["type", "text", "nbInput", "", "placeholder", "Search Class", 1, "w-100", "mw-100", 3, "ngModel", "fieldSize", "ngModelChange"], [1, "mt-1", "mx-2"], [1, "ml-2", 3, "fileName", "data", "exportColumns", "btnSize"], [1, "px-3"], ["style", "height: 550px; border-bottom: 1px solid #ddd; border-right: 1px solid #ddd;", 4, "ngIf"], [2, "height", "550px", "border-bottom", "1px solid #ddd", "border-right", "1px solid #ddd"], ["minBufferPx", "100", "maxBufferPx", "200", 2, "height", "inherit", 3, "itemSize"], [1, "table", "table-bordered", "mb-0"], ["scope", "col", 1, "d-none", "d-sm-table-cell"], ["scope", "col"], [4, "cdkVirtualFor", "cdkVirtualForOf"], ["class", "text-center", 4, "ngIf"], [1, "d-none", "d-sm-table-cell"], [1, "text-center"], ["colspan", "6"]],
      template: function TestCoverageComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nb-card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "nb-card-header", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Apex Test Coverage ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "percent");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "nb-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "nb-icon", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TestCoverageComponent_Template_input_ngModelChange_10_listener($event) {
            return ctx.searchText = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "filter");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "ngx-export", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](15, "filter");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "nb-card-body", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestCoverageComponent_div_17_Template, 21, 6, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" (", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](6, 9, ctx.testCoveragePercentage, "2.2-2"), ") ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchText)("fieldSize", "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](13, 12, ctx.testCoverage, ctx.searchText).length, " row(s)");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fileName", "Apex Test Coverage")("data", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](15, 15, ctx.testCoverage, ctx.searchText))("exportColumns", ctx.exportColumns)("btnSize", "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.testCoverage);
        }
      },
      directives: [_nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbCardComponent"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbCardHeaderComponent"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbTooltipDirective"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbFormFieldComponent"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbIconComponent"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbPrefixDirective"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbInputDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _utilities_export_export_component__WEBPACK_IMPORTED_MODULE_7__["ExportComponent"], _nebular_theme__WEBPACK_IMPORTED_MODULE_5__["NbCardBodyComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_9__["CdkVirtualScrollViewport"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_9__["CdkFixedSizeVirtualScroll"], _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_9__["CdkVirtualForOf"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_8__["PercentPipe"], ng2_search_filter__WEBPACK_IMPORTED_MODULE_10__["Ng2SearchPipe"]],
      styles: ["jqxPanel > div {\n  border: none !important;\n}\n\njqxCheckBox > div {\n  margin-top: 10px !important;\n}\n\n.jqx-widget-content,\n.jqx-dropdownlist {\n  font-family: Open Sans, sans-serif;\n}\n\n.jqx-widget-header {\n  background-color: #fafafa;\n  opacity: 0.9;\n  font-family: Open Sans, sans-serif;\n}\n\n@media screen and (max-width: 576px) {\n  .no-margin-sm {\n    margin-left: 0 !important;\n  }\n  .header {\n    display: block !important;\n    justify-content: center !important;\n    padding: 0.75rem 1.5rem 0.25rem 1.5rem !important;\n  }\n  .header-content {\n    margin-bottom: 0.25rem !important;\n  }\n  table {\n    table-layout: fixed;\n  }\n}\n\nnb-card-header {\n  display: flex;\n  justify-content: space-between;\n  align-items: baseline;\n}\n\n/* #region Custom Scrollbar */\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 2px #f7f9fc;\n  border-radius: 5px !important;\n  background-color: #f7f9fc;\n}\n\n::-webkit-scrollbar {\n  width: 5px;\n  height: 5px;\n  background-color: #f7f9fc;\n}\n\n::-webkit-scrollbar-thumb {\n  border-radius: 5px !important;\n  -webkit-box-shadow: inset 0 0 2px #e4e9f2;\n  background-color: #e4e9f2;\n}\n\n/* #endregion Custom Scrollbar */\n\n.header {\n  display: flex;\n  justify-content: space-between;\n  flex-wrap: wrap;\n}\n\n.header-content {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-wrap: wrap;\n}\n\nth {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n\ntd {\n  overflow-wrap: anywhere;\n}\n\n.header-content label {\n  font-size: 13px;\n}\n\n::-webkit-input-placeholder {\n  /* Chrome/Opera/Safari */\n  font-size: 13px !important;\n}\n\n::-moz-placeholder {\n  /* Firefox 19+ */\n  font-size: 13px !important;\n}\n\n:-ms-input-placeholder {\n  /* IE 10+ */\n  font-size: 13px !important;\n}\n\n:-moz-placeholder {\n  /* Firefox 18- */\n  font-size: 13px !important;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JnLWhlYWx0aC90ZXN0LWNvdmVyYWdlL0M6XFxVc2Vyc1xcU2FpIFNhZ2FyIENDWFxcRG9jdW1lbnRzXFxTa2Vib3RcXFByb2plY3RcXHNrZWJvdC1tZWFuXFxhbmd1bGFyL3NyY1xcYXBwXFxwYWdlc1xcb3JnLWhlYWx0aFxcdGVzdC1jb3ZlcmFnZVxcdGVzdC1jb3ZlcmFnZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvb3JnLWhlYWx0aC90ZXN0LWNvdmVyYWdlL3Rlc3QtY292ZXJhZ2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7QUNDekI7O0FERUE7RUFDRSwyQkFBMkI7QUNDN0I7O0FERUE7O0VBRUUsa0NBQWtDO0FDQ3BDOztBREVBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixrQ0FBa0M7QUNDcEM7O0FERUE7RUFDRTtJQUNFLHlCQUF5QjtFQ0MzQjtFREVBO0lBQ0UseUJBQXlCO0lBQ3pCLGtDQUFrQztJQUNsQyxpREFBaUQ7RUNBbkQ7RURHQTtJQUNFLGlDQUFpQztFQ0RuQztFRElBO0lBQ0UsbUJBQW1CO0VDRnJCO0FBQ0Y7O0FES0E7RUFDRSxhQUFhO0VBQ2IsOEJBQThCO0VBQzlCLHFCQUFxQjtBQ0Z2Qjs7QURLQSw2QkFBQTs7QUFDQTtFQUNFLHlDQUF5QztFQUN6Qyw2QkFBNkI7RUFDN0IseUJBQXlCO0FDRjNCOztBREtBO0VBQ0UsVUFBVTtFQUNWLFdBQVc7RUFDWCx5QkFBeUI7QUNGM0I7O0FES0E7RUFDRSw2QkFBNkI7RUFDN0IseUNBQXlDO0VBQ3pDLHlCQUF5QjtBQ0YzQjs7QURJQSxnQ0FBQTs7QUFFQTtFQUNFLGFBQWE7RUFDYiw4QkFBOEI7RUFFOUIsZUFBZTtBQ0hqQjs7QURNQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLGVBQWU7QUNIakI7O0FETUE7RUFDRSxnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtBQ0hyQjs7QURNQTtFQUNFLHVCQUF1QjtBQ0h6Qjs7QURNQTtFQUNFLGVBQWU7QUNIakI7O0FETUE7RUFDRSx3QkFBQTtFQUNBLDBCQUEwQjtBQ0g1Qjs7QURLQTtFQUNFLGdCQUFBO0VBQ0EsMEJBQTBCO0FDRjVCOztBRElBO0VBQ0UsV0FBQTtFQUNBLDBCQUEwQjtBQ0Q1Qjs7QURHQTtFQUNFLGdCQUFBO0VBQ0EsMEJBQTBCO0FDQTVCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb3JnLWhlYWx0aC90ZXN0LWNvdmVyYWdlL3Rlc3QtY292ZXJhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJqcXhQYW5lbCA+IGRpdiB7XHJcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmpxeENoZWNrQm94ID4gZGl2IHtcclxuICBtYXJnaW4tdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5qcXgtd2lkZ2V0LWNvbnRlbnQsXHJcbi5qcXgtZHJvcGRvd25saXN0IHtcclxuICBmb250LWZhbWlseTogT3BlbiBTYW5zLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4uanF4LXdpZGdldC1oZWFkZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmYWZhZmE7XHJcbiAgb3BhY2l0eTogMC45O1xyXG4gIGZvbnQtZmFtaWx5OiBPcGVuIFNhbnMsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU3NnB4KSB7XHJcbiAgLm5vLW1hcmdpbi1zbSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMCAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLmhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtIDAuMjVyZW0gMS41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyLWNvbnRlbnQge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC4yNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgdGFibGUge1xyXG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcclxuICB9XHJcbn1cclxuXHJcbm5iLWNhcmQtaGVhZGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XHJcbn1cclxuXHJcbi8qICNyZWdpb24gQ3VzdG9tIFNjcm9sbGJhciAqL1xyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCAycHggI2Y3ZjlmYztcclxuICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmOWZjO1xyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogNXB4O1xyXG4gIGhlaWdodDogNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y5ZmM7XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDJweCAjZTRlOWYyO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlNGU5ZjI7XHJcbn1cclxuLyogI2VuZHJlZ2lvbiBDdXN0b20gU2Nyb2xsYmFyICovXHJcblxyXG4uaGVhZGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAvLyBhbGlnbi1pdGVtczogYmFzZWxpbmU7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG59XHJcblxyXG4uaGVhZGVyLWNvbnRlbnQge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbnRoIHtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbn1cclxuXHJcbnRkIHtcclxuICBvdmVyZmxvdy13cmFwOiBhbnl3aGVyZTtcclxufVxyXG5cclxuLmhlYWRlci1jb250ZW50IGxhYmVsIHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XHJcbiAgLyogQ2hyb21lL09wZXJhL1NhZmFyaSAqL1xyXG4gIGZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcbjo6LW1vei1wbGFjZWhvbGRlciB7XHJcbiAgLyogRmlyZWZveCAxOSsgKi9cclxuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcclxufVxyXG46LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAvKiBJRSAxMCsgKi9cclxuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcclxufVxyXG46LW1vei1wbGFjZWhvbGRlciB7XHJcbiAgLyogRmlyZWZveCAxOC0gKi9cclxuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcclxufVxyXG4iLCJqcXhQYW5lbCA+IGRpdiB7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xufVxuXG5qcXhDaGVja0JveCA+IGRpdiB7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbn1cblxuLmpxeC13aWRnZXQtY29udGVudCxcbi5qcXgtZHJvcGRvd25saXN0IHtcbiAgZm9udC1mYW1pbHk6IE9wZW4gU2Fucywgc2Fucy1zZXJpZjtcbn1cblxuLmpxeC13aWRnZXQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcbiAgb3BhY2l0eTogMC45O1xuICBmb250LWZhbWlseTogT3BlbiBTYW5zLCBzYW5zLXNlcmlmO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1NzZweCkge1xuICAubm8tbWFyZ2luLXNtIHtcbiAgICBtYXJnaW4tbGVmdDogMCAhaW1wb3J0YW50O1xuICB9XG4gIC5oZWFkZXIge1xuICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwLjc1cmVtIDEuNXJlbSAwLjI1cmVtIDEuNXJlbSAhaW1wb3J0YW50O1xuICB9XG4gIC5oZWFkZXItY29udGVudCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMC4yNXJlbSAhaW1wb3J0YW50O1xuICB9XG4gIHRhYmxlIHtcbiAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICB9XG59XG5cbm5iLWNhcmQtaGVhZGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XG59XG5cbi8qICNyZWdpb24gQ3VzdG9tIFNjcm9sbGJhciAqL1xuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDJweCAjZjdmOWZjO1xuICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjlmYztcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiA1cHg7XG4gIGhlaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmOWZjO1xufVxuXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDJweCAjZTRlOWYyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlOWYyO1xufVxuXG4vKiAjZW5kcmVnaW9uIEN1c3RvbSBTY3JvbGxiYXIgKi9cbi5oZWFkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmhlYWRlci1jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxudGgge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cblxudGQge1xuICBvdmVyZmxvdy13cmFwOiBhbnl3aGVyZTtcbn1cblxuLmhlYWRlci1jb250ZW50IGxhYmVsIHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAvKiBDaHJvbWUvT3BlcmEvU2FmYXJpICovXG4gIGZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xufVxuXG46Oi1tb3otcGxhY2Vob2xkZXIge1xuICAvKiBGaXJlZm94IDE5KyAqL1xuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcbn1cblxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gIC8qIElFIDEwKyAqL1xuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcbn1cblxuOi1tb3otcGxhY2Vob2xkZXIge1xuICAvKiBGaXJlZm94IDE4LSAqL1xuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcbn1cbiJdfQ== */"],
      encapsulation: 2
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestCoverageComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'ngx-test-coverage',
          templateUrl: './test-coverage.component.html',
          styleUrls: ['./test-coverage.component.scss'],
          encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }]
      }], function () {
        return [{
          type: _core_interfaces_common_testCoverage__WEBPACK_IMPORTED_MODULE_1__["TestCoverageData"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=org-health-org-health-module-es5.js.map