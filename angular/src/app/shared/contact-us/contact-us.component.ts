import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_PATTERN } from '../../@auth/components/constants';
import { ContactUsData, ContactUs } from '../../@core/interfaces/common/contact-us';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'ngx-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {

  maxLength: number = 100;
  contentMaxLength: number = 1000;

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  submitStatus = undefined;

  contactUsForm: FormGroup;

  protected readonly unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private contactUsService: ContactUsData,
  ) {
  }

  get fullName() { return this.contactUsForm.get('fullName'); }
  get email() { return this.contactUsForm.get('email'); }
  get subject() { return this.contactUsForm.get('subject'); }
  get content() { return this.contactUsForm.get('content'); }

  ngOnInit(): void {
    const contentValidators = [
      Validators.maxLength(this.contentMaxLength),
      Validators.required,
    ];
    const fullNameValidators = [
      Validators.maxLength(this.maxLength),
      Validators.required,
    ];
    const subjectValidators = [
      Validators.maxLength(this.maxLength),
      Validators.required,
    ];
    const emailValidators = [
      Validators.pattern(EMAIL_PATTERN),
      Validators.required,
    ];

    this.contactUsForm = this.fb.group({
      content: this.fb.control('', [...contentValidators]),
      email: this.fb.control('', [...emailValidators]),
      fullName: this.fb.control('', [...fullNameValidators]),
      subject: this.fb.control('', [...subjectValidators]),
    });
  }

  submit(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    const msgObj = {
      name: this.fullName.value,
      email: this.email.value,
      message: this.content.value,
    };
    this.submitStatus = undefined;

    this.contactUsService.post(msgObj)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: any) => {
        this.submitStatus = data.success;
        if (data.success) {
          this.contactUsForm.reset();
          // this.contactUsForm.markAsUntouched();
          // this.contactUsForm.markAsPristine();
          // this.contactUsForm.setValue({
          //   fullName: '',
          //   email: '',
          //   subject: '',
          //   content: '',
          // });
        }
      });
  }
}
