import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-shared',
  styleUrls: ['./shared.component.scss'],
  template: `
    <nb-layout>
      <nb-layout-header><b style="font-size: 1.75rem">Skebot</b></nb-layout-header>
      <nb-layout-column class="colored-column-danger">
        <router-outlet></router-outlet>
      </nb-layout-column>
      <nb-layout-footer>
        <div class="footer">
          <div>
            <span class="created-by">
              <b>© {{ currentYear }}
                <a href="https://commercecx.com/" target="_blank" rel="noopener">CommcerceCX  </a>
              </b>
            </span>
          </div>
          <div class="socials">
            <a href="shared/contact-us" title="Contact Us" style="text-decoration:none;">
              <nb-icon icon="email-outline"></nb-icon>
              <span class="pl-1">Contact Us</span>
            </a>
            <a href="https://twitter.com/commercecx" target="_blank" title="Twitter" class="ion ion-social-twitter"></a>
            <a href="https://www.linkedin.com/company/commerce-cx/" title="LinkedIn" target="_blank" class="ion ion-social-linkedin"></a>
          </div>
        </div>
      </nb-layout-footer>
    </nb-layout>
  `,
})
export class SharedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get currentYear(): number {
    return new Date().getFullYear();
  }

}
