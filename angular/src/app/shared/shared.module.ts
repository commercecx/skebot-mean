import { NgModule, ModuleWithProviders } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpRequest } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from '../@core/backend/common/loader.interceptor';

import { ContactUsComponent } from './contact-us/contact-us.component';
import { SharedComponent } from './shared.component';

import {
  NbAlertModule,
  NbCardModule,
  NbIconModule,
  NbLayoutModule,
  NbCheckboxModule,
  NbInputModule,
  NbButtonModule,
  NbSelectModule,
  NbSpinnerModule,
} from '@nebular/theme';
import { SharedRoutingModule } from './shared-routing.module';
import { ComponentsModule } from '../@components/components.module';

const NB_MODULES = [
  NbIconModule,
  NbLayoutModule,
  NbCardModule,
  NbAlertModule,
  NbCheckboxModule,
  NbInputModule,
  NbButtonModule,
  NbSelectModule,
  NbSpinnerModule,
];


@NgModule({
  declarations: [ContactUsComponent, SharedComponent],
  imports: [
    SharedRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    ...NB_MODULES,
    ComponentsModule,
  ],
  providers: [
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
      ],
    };
  }
}
