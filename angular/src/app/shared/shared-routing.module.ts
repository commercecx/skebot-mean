import { ContactUsComponent } from './contact-us/contact-us.component';
import { SharedComponent } from './shared.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [{
  path: '',
  component: SharedComponent,
  children: [
    {
      path: '',
      component: ContactUsComponent,
    },
    {
      path: 'contact-us',
      component: ContactUsComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharedRoutingModule {
}
