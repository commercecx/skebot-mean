import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { ConnectionsComponent } from './connections/connections.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { OrgDashboardComponent } from './org-dashboard/org-dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'connections',
      component: ConnectionsComponent,
    },
    {
      path: 'metadata',
      loadChildren: () => import('./metadata/metadata.module').then(m => m.MetadataModule),
    },
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'orghealth',
      loadChildren: () => import('./org-health/org-health.module').then(m => m.OrgHealthModule),
    },
    {
      path: 'orgdashboard',
      component: OrgDashboardComponent,
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users.module')
        .then(m => m.UsersModule),
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'orgdashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
