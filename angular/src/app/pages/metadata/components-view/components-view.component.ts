import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MetadataData, Metadata } from './../../../@core/interfaces/common/metadata';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { metadataStore, metadataEventDispatcher } from './../../../@core/stores/metadata.store';
import { ActionTypes } from './../../../@core/stores/actions';

@Component({
  selector: 'ngx-components-view',
  templateUrl: './components-view.component.html',
  styleUrls: ['./components-view.component.scss'],
})
export class ComponentsViewComponent implements OnInit, OnDestroy {

  constructor(
    private metadataService: MetadataData,
  ) { }

  ngOnInit(): void {
    metadataStore.subscribe((state) => {
      if (state.mdList.length > 0) {
        this.mdTypeList = state.mdList;
        if (this.mdTypeList[0].components.length === 0)
          this.getComponents(this.mdTypeList[0]);
        else
          this.componentList = this.mdTypeList[0].components;
      }
    });

    metadataEventDispatcher.next({ type: ActionTypes.GET_METADATA });

    if (this.mdTypeList && this.mdTypeList.length > 0)
      this.activeMDName = this.mdTypeList[0].name;
  }

  public ngOnDestroy() {
    this.clearSelections();
  }

  @Input() isPackage: boolean = false;

  protected readonly unsubscribe$ = new Subject<void>();
  mdTypeList: any[] = [];
  selectedCompList: any = [];
  componentList: any[] = [];
  singleSelect: boolean;
  mdSearchText: string;
  compSearchText: string;
  selectedConn: string = '';
  packageName: string = '';
  activeMDName: string = '';
  componentsExportColumns: string[] = ['fullName', 'createdByName', 'lastModifiedDate'];
  metadataExportColumns: string[] = ['name'];

  mdTypeClick(item) {
    this.activeMDName = item.name;
    item.isClicked = true;
    for (let i = 0; i < this.mdTypeList.length; i++) {
      if (this.mdTypeList[i].name !== item.name)
        this.mdTypeList[i].isClicked = false;
    }
    if (item.components.length === 0)
      this.getComponents(item);
    else
      this.componentList = item.components;
  }

  mdTypeSelect(item) {
    this.activeMDName = item.name;
    item.selected = !item.selected;
    item.indeterminate = false;
    item.isClicked = true;
    for (let i = 0; i < this.mdTypeList.length; i++) {
      if (this.mdTypeList[i].name !== item.name)
        this.mdTypeList[i].isClicked = false;
    }
    if (item.components.length === 0) {
      this.getComponents(item);
    } else {
      this.componentList = item.components;
      for (let i = 0; i < item.components.length; i++) {
        item.components[i].selected = item.selected;
      }
    }
  }

  compSelect(comp) {
    comp.selected = !comp.selected;
    if (comp.selected) {
      this.selectedCompList.push({
        'members': comp.fullName,
        'name': comp.type,
      });
    } else {
      let i = 0;
      for (; i < this.selectedCompList.length; i++)
        if (this.selectedCompList[i].members === comp.fullName)
          break;
      this.selectedCompList.splice(i, 1);
    }

    const md = this.mdTypeList.filter(obj => obj.name === this.activeMDName)[0];
    const components = md.components;
    let selectedCompCount = 0;
    for (let i = 0; i < components.length; i++) {
      if (components[i].selected) {
        selectedCompCount++;
      }
    }

    if (selectedCompCount === components.length) {
      md.selected = true;
      md.indeterminate = false;
    } else if (selectedCompCount === 0) {
      md.selected = false;
      md.indeterminate = false;
    } else {
      md.selected = false;
      md.indeterminate = true;
    }
  }

  getComponents(item) {
    const tempData = [];
    const loadComponents = this.metadataService.post([item.name]);
    loadComponents
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          data[keys[i]].selected = item.selected;
          tempData.push(data[keys[i]]);
        }
        item.components = tempData;
        metadataEventDispatcher.next({ type: ActionTypes.GET_METADATA });
        this.componentList = item.components;
      });
  }

  bindComponents() {
    this.componentList = [];
    for (let i = 0; i < this.mdTypeList.length; i++) {
      if (this.mdTypeList[i].selected)
        this.componentList = [...this.componentList, ...this.mdTypeList[i].components];
    }
  }

  clearSelections() {
    this.activeMDName = this.mdTypeList[0].name;
    this.selectedCompList = [];
    for (let i = 0; i < this.mdTypeList.length; i++) {
      const md = this.mdTypeList[i];
      md.selected = false;
      md.indeterminate = false;
      if (md.components.length > 0) {
        const compList = md.components;
        for (let j = 0; j < compList.length; j++) {
          compList[j].selected = false;
        }
      }
    }
  }

}
