import { UtilitiesModule } from './../utilities/utilities.module';
import { Router, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ComponentsComponent } from './components/components.component';
import { MetadataRoutingModule } from './metadata-routing.module';
import { DeployComponent } from './deploy/deploy.component';
import { PackageComponent } from './package/package.component';
// tslint:disable-next-line:max-line-length
import { NbCardModule, NbButtonModule, NbTooltipModule, NbIconModule, NbInputModule, NbTreeGridModule, NbListModule, NbBadgeModule, NbCheckboxModule, NbToggleModule, NbFormFieldModule, NbSelectModule, NbTabsetModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ComponentsViewComponent } from './components-view/components-view.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [ComponentsComponent, DeployComponent, PackageComponent, ComponentsViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    MetadataRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbListModule,
    NbBadgeModule,
    NbCheckboxModule,
    NbToggleModule,
    NbFormFieldModule,
    NbSelectModule,
    NbTabsetModule,
    Ng2SearchPipeModule,
    ThemeModule,
    RouterModule,
    UtilitiesModule,
    ScrollingModule,
  ],
})
export class MetadataModule { }
