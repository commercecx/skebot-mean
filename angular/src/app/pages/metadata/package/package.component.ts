import { metadataStore, metadataEventDispatcher } from './../../../@core/stores/metadata.store';
import { ComponentsViewComponent } from './../components-view/components-view.component';
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { PackageData, Package } from '../../../@core/interfaces/common/package';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { Router } from '@angular/router';
import { ActionTypes } from '../../../@core/stores/actions';

@Component({
  selector: 'ngx-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss'],
})
export class PackageComponent implements AfterViewInit {

  constructor(
    private packageService: PackageData,
    private toastrService: NbToastrService,
    private router: Router,
  ) { }

  @ViewChild(ComponentsViewComponent) componentsView: ComponentsViewComponent;

  protected readonly unsubscribe$ = new Subject<void>();
  packageList: any = [];
  selectedComponents: any;
  selectedCompList: any = [];
  mdSearchText: string;
  compSearchText: string;
  packageName: string = '';
  activeMDTypeIndex: number;
  isPackageListActive: boolean;
  dialogRef: any;
  exportColumns: string[] = ['Title', 'CreatedDate', 'VersionNumber'];

  ngAfterViewInit(): void {
    metadataStore.subscribe((state) => {
      this.packageList = state.packageList;
    });
    metadataEventDispatcher.next({ type: ActionTypes.GET_PACKAGES });
  }

  onChangeTab($event) {
    this.isPackageListActive = ($event.tabTitle === 'Package List' || $event.tabTitle === undefined) ? true : false;
  }

  createPackage(createPackageObj) {
    this.packageService.post(createPackageObj)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: Package) => {
        this.toastrService.success('Status',
          `Package successfully created`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
        );
        this.clear();
        this.loadPackages();
        this.isPackageListActive = true;
      });
  }

  loadPackages() {
    const tempData = [];
    this.packageService.get()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: Package) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          tempData.push(data[keys[i]]);
        }
        metadataEventDispatcher.next({ type: ActionTypes.SET_PACKAGES, payload: tempData });
      });
  }

  validateAndCreatePackage() {
    const titleList = this.packageList.map(p => p.Title.trim());
    this.selectedCompList = this.componentsView.selectedCompList;
    if (this.packageName === '') {
      this.toastrService.warning('Mandatory Fields',
        `Please enter Package Name`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else if (this.selectedCompList.length === 0) {
      this.toastrService.warning('Mandatory Fields',
        `Please select atleast one component to create a Package`,
        { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else if (titleList.indexOf(this.packageName.trim()) > -1) {
      this.toastrService.warning('Mandatory Fields',
        `Package Name already exists. Please enter unique name.`,
        { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else {
      const createPackageObj = {
        packageName: this.packageName.trim(),
        compList: this.selectedCompList,
      };
      this.createPackage(createPackageObj);
      this.packageName = '';
      this.selectedCompList = [];
    }
  }

  clear() {
    this.packageName = '';
    this.selectedCompList = [];
    this.componentsView.clearSelections();
  }

  deploy(packageId) {
    this.router.navigateByUrl('/pages/metadata/deploy?packageId=' + packageId);
  }

}
