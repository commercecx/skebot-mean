import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MetadataData, Metadata } from './../../../@core/interfaces/common/metadata';
import { ConnectionsData, Connection } from './../../../@core/interfaces/common/connections';
import { NbToastrService, NbGlobalPhysicalPosition, NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss'],
})
export class ComponentsComponent implements OnInit {

  constructor(
    private metadataService: MetadataData,
    private connectionsService: ConnectionsData,
    private toastrService: NbToastrService,
    // protected dialogRef: NbDialogRef<ComponentsComponent>,
  ) { }

  @Input() isPackage: boolean = false;
  // @Output() createPackageEvent = new EventEmitter();
  // @Output() loadPackagesEvent = new EventEmitter();

  protected readonly unsubscribe$ = new Subject<void>();
  mdTypeList: any[] = [];
  selectedCompList: any = [];
  componentList: any[] = [];
  singleSelect: boolean;
  mdSearchText: string;
  compSearchText: string;
  connList: Connection[];
  selectedConn: string = '';
  packageName: string = '';
  activeMDTypeIndex: number;

  ngOnInit(): void {
    // this.connectionsService.changeConn('', 'source');
    // this.connectionsService.changeConn('', 'dest');
    // this.loadConnectionsData();
    // this.loadMetadataData();
  }

  // loadConnectionsData() {
  //   this.connectionsService.get()
  //     .pipe(takeUntil(this.unsubscribe$))
  //     .subscribe((data: Connection) => {
  //       this.connList = [];
  //       const keys = Object.keys(data);
  //       for (let i = 0; i < keys.length; i++) {
  //         this.connList.push(data[keys[i]]);
  //       }
  //     });
  // }

  // loadMetadataData() {
  //   this.metadataService.get()
  //     .pipe(takeUntil(this.unsubscribe$))
  //     .subscribe((data) => {
  //       const keys = Object.keys(data);
  //       for (let i = 0; i < keys.length; i++) {
  //         this.mdTypeList.push(data[keys[i]]);
  //       }
  //       for (let i = 0; i < this.mdTypeList.length; i++) {
  //         this.mdTypeList[i].components = [];
  //         this.mdTypeList[i].indeterminate = false;
  //       }
  //       this.activeMDTypeIndex = 0;
  //       this.getComponents(this.mdTypeList[0]);
  //     });
  // }

  // mdTypeClick(item, index) {
  //   this.activeMDTypeIndex = index;
  //   if (item.components.length === 0)
  //     this.getComponents(item);
  //   else
  //     this.componentList = item.components;
  // }

  // mdTypeSelect(item, index) {
  //   this.activeMDTypeIndex = index;
  //   item.selected = !item.selected;
  //   item.indeterminate = false;
  //   if (item.components.length === 0) {
  //     this.getComponents(item);
  //   } else
  //     this.componentList = item.components;

  //   for (let i = 0; i < item.components.length; i++) {
  //     item.components[i].selected = item.selected;
  //   }
  // }

  // compSelect(comp) {
  //   comp.selected = !comp.selected;
  //   if (comp.selected) {
  //     this.selectedCompList.push({
  //       'members': comp.fullName,
  //       'name': comp.type,
  //     });
  //   } else {
  //     let i = 0;
  //     for (; i < this.selectedCompList.length; i++)
  //       if (this.selectedCompList[i].members === comp.fullName)
  //         break;
  //     this.selectedCompList.splice(i, 1);
  //   }

  //   const md = this.mdTypeList[this.activeMDTypeIndex];
  //   const components = this.mdTypeList[this.activeMDTypeIndex].components;
  //   let selectedCompCount = 0;
  //   for (let i = 0; i < components.length; i++) {
  //     if (components[i].selected) {
  //       selectedCompCount++;
  //     }
  //   }

  //   if (selectedCompCount === components.length) {
  //     md.selected = true;
  //     md.indeterminate = false;
  //   } else if (selectedCompCount === 0) {
  //     md.selected = false;
  //     md.indeterminate = false;
  //   } else {
  //     md.selected = false;
  //     md.indeterminate = true;
  //   }
  // }

  // getComponents(item) {
  //   const loadComponents = this.metadataService.post([item.name]);
  //   loadComponents
  //     .pipe(takeUntil(this.unsubscribe$))
  //     .subscribe((data) => {
  //       const keys = Object.keys(data);
  //       for (let i = 0; i < keys.length; i++) {
  //         data[keys[i]].selected = false;
  //         item.components.push(data[keys[i]]);
  //       }
  //       this.componentList = item.components;
  //     });
  // }

  // bindComponents() {
  //   this.componentList = [];
  //   for (let i = 0; i < this.mdTypeList.length; i++) {
  //     if (this.mdTypeList[i].selected)
  //       this.componentList = [...this.componentList, ...this.mdTypeList[i].components];
  //   }
  // }

  // changeOrg() {
  //   this.connectionsService.changeConn(this.selectedConn, 'source');
  //   this.loadMetadataData();
  // }

  // validateAndCreatePackage() {
  //   if (this.packageName === '') {
  //     this.toastrService.warning('Mandatory Fields',
  //       `Please enter Package Name`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
  //     );
  //   } else if (this.selectedCompList.length === 0) {
  //     this.toastrService.warning('Mandatory Fields',
  // tslint:disable-next-line:max-line-length
  //       `Please select atleast one component to create a Package`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
  //     );
  //   } else {
  //     const createPackageObj = {
  //       orgId: this.selectedConn,
  //       packageName: this.packageName,
  //       compList: this.selectedCompList,
  //       status: 'Submitted',
  //     };
  //     // this.createPackageEvent.emit(createPackageObj);
  //     // this.loadMetadataData();
  //     // this.packageName = '';
  //     // this.selectedConn = '';
  //     // this.selectedCompList = [];
  //     this.dialogRef.close(createPackageObj);
  //   }
  // }

  // cancel() {
  //   // this.loadMetadataData();
  //   // this.packageName = '';
  //   // this.selectedConn = '';
  //   // this.selectedCompList = [];
  //   const createPackageObj = {
  //     status: 'Cancelled',
  //   };
  //   this.dialogRef.close(createPackageObj);
  // }
}
