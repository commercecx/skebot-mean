import { ActionTypes } from './../../../@core/stores/actions';
import { metadataStore, metadataEventDispatcher } from './../../../@core/stores/metadata.store';
import { Router, ActivatedRoute } from '@angular/router';
import { DeployData } from '../../../@core/interfaces/common/deployment';
import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { connectionEventDispatcher, connectionStore } from '../../../@core/stores/connection.store';

@Component({
  selector: 'ngx-deploy',
  templateUrl: './deploy.component.html',
  styleUrls: ['./deploy.component.scss'],
})
export class DeployComponent implements OnInit {

  constructor(
    private toastrService: NbToastrService,
    private deployService: DeployData,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  protected readonly unsubscribe$ = new Subject<void>();
  connList: any = [];
  deployHistory: any = [];
  selectedPackage: string = '';
  destConn: string = '';

  packageList: any = [];

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.selectedPackage = params['packageId'];
    });

    metadataStore.subscribe((state) => {
      this.packageList = state.packageList;
    });

    connectionStore.subscribe((state) => {
      this.connList = state.connections;
    });

    metadataEventDispatcher.next({ type: ActionTypes.GET_PACKAGES });
    connectionEventDispatcher.next({ type: ActionTypes.GET_CONNECTIONS });
  }

  changeDestOrg() {
    connectionEventDispatcher.next({ type: ActionTypes.CHANGE_DEST_CONNECTION, payload: this.destConn });
    metadataEventDispatcher.next({ type: ActionTypes.SET_PACKAGES });
  }

  addOrg() {
    this.router.navigateByUrl('/pages/connections');
  }

  validate(isValidate) {
    if (this.selectedPackage === '') {
      this.toastrService.warning('Mandatory Fields',
        `Please select Package to deploy`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else if (this.destConn === '') {
      this.toastrService.warning('Mandatory Fields',
        `Please select Destination Org`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else {
      this.deployPackage(isValidate);
    }
  }

  deployPackage(isValidate) {
    this.deployService.post(this.selectedPackage, isValidate)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: any) => {
        this.selectedPackage = '';
        this.destConn = '';
        if (data && data.status === 'Failed') {
          this.toastrService.warning('Status',
            isValidate ? `Validation Failed` : `Deployment Failed`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
          );
        } else {
          this.toastrService.success('Status',
            (isValidate ? `Validation` : `Deployment`) + ' Successful',
            { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
          );
        }
      });
  }

}
