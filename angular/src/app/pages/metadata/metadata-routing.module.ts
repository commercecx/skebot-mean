import { ComponentsComponent } from './components/components.component';
import { PackageComponent } from './package/package.component';
import { DeployComponent } from './deploy/deploy.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'components',
    component: ComponentsComponent,
  },
  {
    path: 'package',
    component: PackageComponent,
  },
  {
    path: 'deploy',
    component: DeployComponent,
  },
  { path: '', redirectTo: 'components', pathMatch: 'full' },
  { path: '**', redirectTo: 'components' },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
  ],
})
export class MetadataRoutingModule { }
