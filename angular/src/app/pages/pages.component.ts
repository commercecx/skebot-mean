import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeWhile, map, filter } from 'rxjs/operators';
import { NbTokenService } from '@nebular/auth';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';
import { PagesMenu } from './pages-menu';
import { InitUserService } from '../@theme/services/init-user.service';
import { LayoutService } from '../@core/utils';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-loader></ngx-loader>
    <ngx-one-column-layout>
      <nb-menu [items]="menu" [tag]="'sidebar-menu'"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})

export class PagesComponent implements OnInit, OnDestroy {

  menu: NbMenuItem[];
  alive: boolean = true;

  constructor(private pagesMenu: PagesMenu,
    private tokenService: NbTokenService,
    protected initUserService: InitUserService,
    private nbMenuService: NbMenuService,
    private sidebarService: NbSidebarService,
    private layoutService: LayoutService,
  ) {
    this.initMenu();

    this.tokenService.tokenChange()
      .pipe(takeWhile(() => this.alive))
      .subscribe(() => {
        this.initMenu();
      });
  }

  initMenu() {
    this.pagesMenu.getMenu()
      .pipe(takeWhile(() => this.alive))
      .subscribe(menu => {
        this.menu = menu;
      });
  }

  ngOnInit() {
    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'sidebar-menu'),
        map(({ item: item }) => item),
      )
      .subscribe(item => {
        if (window.innerWidth < 576) {
          this.sidebarService.toggle(true, 'menu-sidebar');
          this.layoutService.changeLayoutSize();
        }
      });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
