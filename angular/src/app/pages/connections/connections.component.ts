import { ActionTypes } from './../../@core/stores/actions';
import { connectionStore, connectionEventDispatcher } from './../../@core/stores/connection.store';
import { ConnectionsData, Connection } from './../../@core/interfaces/common/connections';
import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../@auth/authentication.service';
import { NbToastrService, NbGlobalPhysicalPosition, NbDialogService } from '@nebular/theme';
import { AddConnectionComponent } from './add-connection/add-connection.component';

@Component({
  selector: 'ngx-connections',
  styleUrls: ['./connections.component.scss'],
  templateUrl: './connections.component.html',
})
export class ConnectionsComponent implements OnInit {

  constructor(
    private connectionsService: ConnectionsData,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,
  ) {
  }

  protected readonly unsubscribe$ = new Subject<void>();

  connList = [];
  exportColumns: string[] = ['name', 'orgType', 'username'];

  ngOnInit() {
    this.connectionsService.changeConn('', 'source');
    this.connectionsService.changeConn('', 'dest');
    this.route.queryParams.subscribe(params => {
      let code = params['code'];
      const state = params['state'];
      if (code && state) { // This scenario is when user adds a new connection
        const kvPairList = state.split('&');
        for (let i = 0; i < kvPairList.length; i++) {
          kvPairList[i] = kvPairList[i].replace(':', '=');
        }
        const envmnt = kvPairList.join('&');
        code = 'code=' + code;
        this.authService.authorizeConnections(code, envmnt)
          .subscribe(() => {
            this.toastrService.success('Success Message',
              `Connection added successfully`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT });
            this.getConnections();
          });
      } else { // This scenario is to get the connections for the first time
        // this.getConnections();
      }
    });

    connectionStore.subscribe((state) => {
      this.connList = state.connections;
    });

    connectionEventDispatcher.next({ type: ActionTypes.GET_CONNECTIONS });
  }

  getConnections() {
    const tempData = [];
    const loadConnectionsData = this.connectionsService.get();
    loadConnectionsData
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: Connection) => {
        tempData.push({
          _id: '',
          name: 'LoggedIn Org',
          title: 'LoggedIn Org',
        });
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          data[keys[i]]['title'] = data[keys[i]]['name'];
          tempData.push(data[keys[i]]);
        }
        connectionEventDispatcher.next({ type: ActionTypes.SET_CONNECTIONS, payload: tempData });
      });
  }

  deleteConn(id: string) {
    this.connectionsService.delete(id)
      .subscribe((data: string) => {
        if (data === 'deleted successfully') {
          this.toastrService.success('Success Message',
            `Connection deleted successfully`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT });
          // this.getConnections();
          connectionEventDispatcher.next({ type: ActionTypes.DELETE_CONNECTION, payload: id });
        } else {
          this.toastrService.danger('Failure Message',
            `Unable to delete Connection. Plesse contact support`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT });
        }
      });
  }

  addConn() {
    this.dialogService.open(AddConnectionComponent)
      .onClose.subscribe((status) => {
        if (status === 'success') {
          this.getConnections();
        }
      });
  }
}
