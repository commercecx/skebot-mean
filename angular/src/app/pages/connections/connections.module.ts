import { UtilitiesModule } from './../utilities/utilities.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ConnectionsComponent } from './connections.component';
import {
  NbCardModule,
  NbListModule,
  NbInputModule,
  NbSelectModule,
  NbIconModule,
  NbButtonModule,
  NbAccordionModule,
  NbTooltipModule,
} from '@nebular/theme';
import { AddConnectionComponent } from './add-connection/add-connection.component';

@NgModule({
  declarations: [ConnectionsComponent, AddConnectionComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbListModule,
    NbInputModule,
    NbSelectModule,
    NbIconModule,
    NbButtonModule,
    NbAccordionModule,
    NbTooltipModule,
    UtilitiesModule,
  ],
  providers: [
  ],
})
export class ConnectionsModule { }
