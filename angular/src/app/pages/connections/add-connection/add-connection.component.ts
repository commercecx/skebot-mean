import { Component, OnInit } from '@angular/core';
import { NbToastrService, NbGlobalPhysicalPosition, NbDialogRef } from '@nebular/theme';
import { environment } from '../../../../../src/environments/environment';
@Component({
  selector: 'ngx-add-connection',
  templateUrl: './add-connection.component.html',
  styleUrls: ['./add-connection.component.scss'],
})
export class AddConnectionComponent implements OnInit {

  constructor(
    private toastrService: NbToastrService,
    protected ref: NbDialogRef<AddConnectionComponent>,
  ) { }

  ngOnInit(): void {
  }

  connectionName: string;
  selectedOrg: string;

  authorize() {
    if ((!this.connectionName ||
      this.connectionName.trim() === '') &&
      (!this.selectedOrg ||
        this.selectedOrg.trim() === '')) {
      this.toastrService.warning('Mandatory Fields',
        `Please enter Connection Name and select Org`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else if (!this.selectedOrg || this.selectedOrg.trim() === '') {
      this.toastrService.warning('Mandatory Fields',
        `Please select Org`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT },
      );
    } else if (!this.connectionName || this.connectionName.trim() === '') {
      this.toastrService.warning('Mandatory Fields',
        `Please enter Connection Name`, { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT });
    } else {
      const tempConnName = this.connectionName.split(' ').join('');
      if (this.selectedOrg === 'Production')
        window.location.href = `${environment.apiUrl}/auth/connection/login/prod/${tempConnName}`;
      else if (this.selectedOrg === 'Sandbox')
        window.location.href = `${environment.apiUrl}/auth/connection/login/sandbox/${tempConnName}`;
    }
  }

  cancel() {
    this.selectedOrg = '';
    this.connectionName = '';
    this.ref.close('Cancelled');
  }

}
