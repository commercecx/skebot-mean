import { Component, OnInit, Input } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'ngx-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss'],
})
export class ExportComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() fileName: string = '';
  @Input() data: any;
  @Input() exportColumns: string[];
  @Input() btnSize: string = 'medium';
  @Input() iconOnly: boolean = false;
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };

  export(): void {
    if (this.data.length > 0) {
      /* generate worksheet */
      const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.jsonToAOA());

      /* generate workbook and add the worksheet */
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, this.fileName);

      /* save to file */
      XLSX.writeFile(wb, this.fileName + '.xlsx');
    }
  }

  // Convert JSON array to AOA
  jsonToAOA() {
    if (this.exportColumns && this.exportColumns.length > 0)
      this.deleteExtraColumns();
    const exportDataAOA = [];
    exportDataAOA.push(this.modifyColumnNames());
    for (let i = 0; i < this.data.length; i++) {
      exportDataAOA.push(Object.values(this.data[i]));
    }
    return exportDataAOA;
  }

  deleteExtraColumns() {
    const allColumns = Object.keys(this.data[0]);
    const that = this;
    allColumns.forEach(function (key) {
      if (that.exportColumns.indexOf(key) === -1)
        that.data.forEach(function (v) { delete v[key]; });
    });
  }

  modifyColumnNames() {
    const columns = Object.keys(this.data[0]);
    for (let i = 0; i < columns.length; i++) {
      columns[i] = columns[i].charAt(0).toUpperCase() + columns[i].substr(1).replace(/([A-Z]+)/g, ' $1');
    }
    return columns;
  }

}
