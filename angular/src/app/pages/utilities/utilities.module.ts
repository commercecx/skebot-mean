import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { ExportComponent } from './export/export.component';
import {
  NbIconModule,
  NbButtonModule,
} from '@nebular/theme';

@NgModule({
  declarations: [LoaderComponent, ExportComponent],
  imports: [
    CommonModule,
    NbIconModule,
    NbButtonModule,
  ],
  exports: [LoaderComponent, ExportComponent],
})
export class UtilitiesModule { }
