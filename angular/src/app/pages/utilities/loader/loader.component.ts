import { Component, OnInit, Input } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { LoaderService } from '../../../@core/backend/common/services/loader.service';

@Component({
  selector: 'ngx-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent {

  @Input() message: BehaviorSubject<String> = this.loaderService.message;
  isLoading: BehaviorSubject<boolean> = this.loaderService.isLoading;
  constructor(private loaderService: LoaderService) {
  }

}
