import { NbMenuItem } from '@nebular/theme';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {

  getMenu(): Observable<NbMenuItem[]> {

    const menu: NbMenuItem[] = [
      {
        title: 'Dashboard',
        icon: 'pie-chart-outline',
        link: '/pages/orgdashboard',
      },
      {
        icon: 'activity-outline',
        title: 'Apex Test Coverage',
        link: '/pages/orghealth/testcoverage',
      },
      {
        icon: 'file-outline',
        title: 'Components',
        link: '/pages/metadata/components',
      },
      {
        icon: 'folder-add-outline',
        title: 'Package',
        link: '/pages/metadata/package',
      },
      {
        icon: 'cloud-upload-outline',
        title: 'Deploy',
        link: '/pages/metadata/deploy',
      },
      {
        title: 'Connections',
        icon: 'swap-outline',
        link: '/pages/connections',
      },
    ];

    return of([...menu]);
  }
}
