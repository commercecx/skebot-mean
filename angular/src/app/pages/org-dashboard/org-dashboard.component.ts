import { dashboardStore, dashboardEventDispatcher } from './../../@core/stores/dashboard.store';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { ActionTypes } from '../../@core/stores/actions';

@Component({
  selector: 'ngx-org-dashboard',
  templateUrl: './org-dashboard.component.html',
  styleUrls: ['./org-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrgDashboardComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit(): void {
    dashboardStore.subscribe((state) => {
      this.loginHistoryList = state.loginHistory;
      this.auditHistoryList = state.auditHistory;
      this.limitsList = state.limitsList;
    });

    dashboardEventDispatcher.next({ type: ActionTypes.GET_LOGINHISTORY });
    dashboardEventDispatcher.next({ type: ActionTypes.GET_AUDITHISTORY });
    dashboardEventDispatcher.next({ type: ActionTypes.GET_LIMITS });
  }

  protected readonly unsubscribe$ = new Subject<void>();
  loginHistoryList: any = [];
  loginHistoryGroupBy: string[] = ['Status', 'Browser', 'LoginType', 'Application', 'Country'];
  loginHistoryChartTypes: string[] = ['Bar', 'Column', 'Pie', 'Doughnut'];
  loginHistoryChartTitle: string = 'Login History';
  loginHistoryExportColumns: string[] = ['ApiType', 'Browser', 'Status', 'Application', 'LoginTime', 'LoginType'];
  auditHistoryList: any = [];
  auditHistoryGroupBy: string[] = ['Action', 'Display', 'CreatedBy'];
  auditHistoryChartTypes: string[] = ['Bar', 'Column', 'Pie', 'Doughnut'];
  auditHistoryChartTitle: string = 'Audit History';
  auditHistoryExportColumns: string[] = ['CreatedDate', 'CreatedBy', 'DelegateUser', 'Action', 'Display'];
  limitsList: any = [];
}
