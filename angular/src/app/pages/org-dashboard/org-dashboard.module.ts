import { UtilitiesModule } from './../utilities/utilities.module';
import { NbCardModule, NbSelectModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrgDashboardComponent } from './org-dashboard.component';
import { HistoryComponent } from './history/history.component';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { OrgUsageComponent } from './org-usage/org-usage.component';
import { ChartModule } from 'angular2-chartjs';

@NgModule({
  declarations: [OrgDashboardComponent, HistoryComponent, OrgUsageComponent],
  imports: [
    CommonModule,
    ChartsModule,
    NgxChartsModule,
    NbCardModule,
    NbSelectModule,
    ChartModule,
    UtilitiesModule,
  ],
  exports: [
    HistoryComponent,
  ],
})
export class OrgDashboardModule {
}
