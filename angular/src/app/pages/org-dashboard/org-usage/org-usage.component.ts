import { Component, OnChanges, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-org-usage',
  templateUrl: './org-usage.component.html',
  styleUrls: ['./org-usage.component.scss'],
})
export class OrgUsageComponent implements OnChanges {

  constructor() { }

  ngOnChanges(): void {
    if (this.limitsData.length > 0)
      this.chartList = this.limitsData;
  }

  @Input() limitsData: any = [];
  chartList: any[] = [];

}
