import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgUsageComponent } from './org-usage.component';

describe('OrgUsageComponent', () => {
  let component: OrgUsageComponent;
  let fixture: ComponentFixture<OrgUsageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgUsageComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgUsageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
