import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnChanges {

  constructor() { }

  @Input() data: any;
  @Input() chartTitle: string;
  @Input() groupByList: string[];
  @Input() chartTypeList: string[];
  @Input() defaultChart: string;
  @Input() exportColumns: string[];
  selectedGroup: string;
  selectedChartType: string;

  chartObj: any = {
    data: [],
    showLegend: false,
    showXAxis: true,
    showYAxis: true,
    showXAxisLabel: true,
    showYAxisLabel: true,
    xAxisLabel: this.selectedGroup,
    yAxisLabel: this.selectedGroup + ' Count',
    showLabels: true,
    showDataLabel: true,
    colorScheme: {
      domain: [
        '#CD6155', '#AF7AC5', '#5499C7', '#48C9B0',
        '#52BE80', '#F4D03F', '#EB984E', '#AAB7B8',
        '#5D6D7E', '#03A9F4', '#37474F', '#FDD835', '#FF6F00'],
    },
  };

  ngOnChanges(): void {
    if (this.data.length > 0) {
      this.selectedGroup = this.groupByList[0];
      this.selectedChartType = this.defaultChart;
      this.createChart();
    } else
      return;
  }

  compare(a, b) {
    let comparison = 0;
    if (a.value < b.value) {
      comparison = 1;
    } else if (a.value > b.value) {
      comparison = -1;
    }
    return comparison;
  }

  modifyObj(data) {
    const tempObj = {
      name: 'Others',
      value: 0,
    };
    for (let i = 11; i < data.length; i++) {
      tempObj.value += data[i].value;
    }
    data = data.slice(0, 12);
    data.push(tempObj);
    return data;
  }

  createChart() {
    let data = [];
    const tempData = this.data.map(a => a[this.selectedGroup]);
    for (let i = 0; i < tempData.length; i++) {
      let key = tempData[i];
      key = (key === null || key === undefined) ? 'Others' : key;
      const keys = data.map(a => a.name);
      const index = keys.indexOf(key);
      if (index > -1) {
        data[index].value = data[index].value + 1;
      } else {
        data.push({ 'name': key, value: 1 });
      }
    }
    data.sort(this.compare); // Sort Descending
    if (data.length > 12)
      data = this.modifyObj(data); // Restrict to 13 Values to Display
    this.chartObj.data = data;
  }
}
