import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrgHealthComponent } from './org-health.component';
import { TestCoverageComponent } from './test-coverage/test-coverage.component';
const routes: Routes = [
  {
    path: '',
    component: OrgHealthComponent,
    children: [
      {
        path: 'testcoverage',
        component: TestCoverageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrgHealthRoutingModule { }
