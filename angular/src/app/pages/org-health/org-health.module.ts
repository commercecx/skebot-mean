import { ScrollingModule } from '@angular/cdk/scrolling';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UtilitiesModule } from './../utilities/utilities.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrgHealthRoutingModule } from './org-health-routing.module';
import { OrgHealthComponent } from './org-health.component';
import { TestCoverageComponent } from './test-coverage/test-coverage.component';
// tslint:disable-next-line:max-line-length
import { NbCardModule, NbButtonModule, NbTooltipModule, NbIconModule, NbInputModule, NbTreeGridModule, NbFormFieldModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  declarations: [OrgHealthComponent, TestCoverageComponent],
  imports: [
    CommonModule,
    FormsModule,
    OrgHealthRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbFormFieldModule,
    ThemeModule,
    NgxEchartsModule,
    UtilitiesModule,
    Ng2SearchPipeModule,
    ScrollingModule,
  ],
})
export class OrgHealthModule { }
