import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-org-health',
  template: `<router-outlet></router-outlet>`,
})
export class OrgHealthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
