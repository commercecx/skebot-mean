// tslint:disable:indent
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TestCoverageData } from '../../../@core/interfaces/common/testCoverage';
import { Subject } from 'rxjs/Subject';
import { dashboardStore, dashboardEventDispatcher } from '../../../@core/stores/dashboard.store';
import { ActionTypes } from '../../../@core/stores/actions';

@Component({
	selector: 'ngx-test-coverage',
	templateUrl: './test-coverage.component.html',
	styleUrls: ['./test-coverage.component.scss'],
	encapsulation: ViewEncapsulation.None,
})

export class TestCoverageComponent implements OnInit {
	protected readonly unsubscribe$ = new Subject<void>();
	testCoveragePercentage: number = 0;
	testCoverage: any = [];
	searchText: string = '';
	exportColumns: string[] = [
		'ApexClassOrTriggerId',
		'ApexClassOrTrigger',
		'NumLinesCovered',
		'NumLinesUncovered',
		'TotalPercentage',
		'LastModifiedBy',
	];

	constructor(private testCoverageService: TestCoverageData) {
	}

	ngOnInit() {
		dashboardStore.subscribe((state) => {
			this.testCoverage = state.testCoverage;
			this.testCoveragePercentage = state.testCoveragePercentage;
		});
		dashboardEventDispatcher.next({ type: ActionTypes.GET_TESTCOVERAGE });
		dashboardEventDispatcher.next({ type: ActionTypes.GET_TESTCOVERAGEPERCENTAGE });
	}
}
