import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCoverageComponent } from './test-coverage.component';

describe('TestCoverageComponent', () => {
  let component: TestCoverageComponent;
  let fixture: ComponentFixture<TestCoverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCoverageComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCoverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
