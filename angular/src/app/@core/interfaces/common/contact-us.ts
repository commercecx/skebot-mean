import { Observable } from 'rxjs';

export interface ContactUs {
  // [x: string]: any;
  name: string;
  email: string;
  message: string;
}

export abstract class ContactUsData {
  abstract post(msgObj): Observable<ContactUs>;
}
