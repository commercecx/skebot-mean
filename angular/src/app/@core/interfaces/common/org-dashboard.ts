import { Observable } from 'rxjs';

export interface LoginObj {
  attributes: {
    type: string;
    url: string;
  };
  Id: string;
  ApiType: string;
  Browser: string;
  Status: string;
  LoginGeo: {
    attributes: {
      type: string;
      url: string;
    };
    Country: string;
  };
  Application: string;
  LoginTime: string;
  LoginType: string;
}

export interface AuditObj {
  attributes: {
    type: string;
    url: string;
  };
  Id: string;
  CreatedDate: string;
  CreatedBy: {
    attributes: {
      type: string;
      url: string;
    };
    Username: string;
  };
  DelegateUser: string;
  Action: string;
  Display: string;
}

export abstract class OrgDashboardData {
  abstract getLoginHistory(): Observable<LoginObj>;
  abstract getAuditHistory(): Observable<AuditObj>;
  abstract getLimits(): Observable<any>;
}
