import { Observable } from 'rxjs';

export interface TestCoverage {
  ApexClassOrTriggerId: string;
  name: string;
  NumLinesCovered: number;
  NumLinesUncovered: number;
}


export abstract class TestCoverageData {
  abstract get(): Observable<TestCoverage>;
}
