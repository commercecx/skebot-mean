import { Observable } from 'rxjs';

export interface Package {
  // [x: string]: any;
  attributes: {
    type: string,
    url: string,
  };
  Id: string;
  Title: string;
  TagCsv: string;
  CreatedBy: {
    attributes: {
      type: string,
      url: string,
    },
    Name: string,
  };
  VersionNumber: string;
  CreatedDate: string;
  VersionData: string;
}

export abstract class PackageData {
  abstract get(): Observable<Package>;
  abstract post(createPackageObj: any): Observable<Package>;
  abstract delete(id: string): Observable<string>;
  abstract sourceConnId: string;
}
