import { Observable } from 'rxjs';

export abstract class DeployData {
  abstract post(packageName: string, isValidate: boolean): Observable<any>;
}
