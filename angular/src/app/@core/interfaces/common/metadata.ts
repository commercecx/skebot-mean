import { Observable } from 'rxjs';

export interface Metadata {
  // [x: string]: any;
  name: string;
  selected: boolean;
  indeterminate: boolean;
  directoryName: string;
  components: any;
}

export abstract class MetadataData {
  abstract get(): Observable<Metadata>;
  abstract post(mdName: string[]): Observable<Metadata>;
}
