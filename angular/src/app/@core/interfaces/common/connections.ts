import { Observable } from 'rxjs';

export interface Connection {
  // [x: string]: any;
  _id: string;
  name: string;
  orgType: string;
  username: string;
  title: string;
}

export abstract class ConnectionsData {
  abstract get(): Observable<Connection>;
  abstract delete(id: string): Observable<string>;
  abstract changeConn(connId: string, type: string): void;
  abstract getConnId(type: string): string;
}
