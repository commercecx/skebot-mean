import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MetadataApi } from '../api/metadata.api';
import { Metadata, MetadataData } from '../../../interfaces/common/metadata';

@Injectable()
export class MetadataService extends MetadataData {

  constructor(private api: MetadataApi) {
    super();
  }

  get(): Observable<Metadata> {
    return this.api.get();
  }

  post(mdType): Observable<Metadata> {
    return this.api.post(mdType);
  }
}
