import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConnectionsApi } from '../api/connections.api';
import { Connection, ConnectionsData } from '../../../interfaces/common/connections';

@Injectable()
export class ConnectionsService extends ConnectionsData {

  constructor(private api: ConnectionsApi) {
    super();
  }

  sourceConnId: string = '';
  destConnId: string = '';

  get(): Observable<Connection> {
    const data = this.api.get();
    return data;
  }

  delete(id: string): Observable<string> {
    return this.api.delete(id);
  }

  changeConn(connId, type) {
    if (type === 'source')
      this.sourceConnId = connId;
    else
      this.destConnId = connId;
  }

  getConnId(type) {
    if (type === 'source')
      return this.sourceConnId;
    else
      return this.destConnId;
  }

}
