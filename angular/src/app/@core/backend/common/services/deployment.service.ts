import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DeployApi } from '../api/deployment.api';
import {  DeployData } from '../../../interfaces/common/deployment';

@Injectable()
export class DeployService extends DeployData {

  constructor(private api: DeployApi) {
    super();
  }

  post(packageName: string, isValidate: boolean): Observable<any> {
    return this.api.post(packageName, isValidate);
  }
}
