import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PackageApi } from '../api/package.api';
import { Package, PackageData } from '../../../interfaces/common/package';

@Injectable()
export class PackageService extends PackageData {

  constructor(private api: PackageApi) {
    super();
  }

  sourceConnId: string = '';
  destConnId: string = '';

  get(): Observable<Package> {
    const data = this.api.get();
    return data;
  }

  post(compList): Observable<Package> {
    const data = this.api.post(compList);
    return data;
  }

  delete(id: string): Observable<string> {
    return this.api.delete(id);
  }

}
