import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {

  isLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);
  message: BehaviorSubject<String> = new BehaviorSubject('Please wait...');
  show() {
    this.isLoading.next(true);
  }
  hide() {
    this.isLoading.next(false);
  }
}
