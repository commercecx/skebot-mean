import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TestCoverageApi } from '../api/testCoverage.api';
import { TestCoverage, TestCoverageData } from '../../../interfaces/common/testCoverage';

@Injectable()
export class TestCoverageService extends TestCoverageData {

  constructor(private api: TestCoverageApi) {
    super();
  }

  get(): Observable<TestCoverage> {
    return this.api.get();
  }

}
