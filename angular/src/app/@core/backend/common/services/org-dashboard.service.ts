import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OrgDashboardApi } from '../api/org-dashboard.api';
import { LoginObj, AuditObj, OrgDashboardData } from '../../../interfaces/common/org-dashboard';

@Injectable()
export class OrgDashboardService extends OrgDashboardData {

  constructor(private api: OrgDashboardApi) {
    super();
  }

  getLoginHistory(): Observable<LoginObj> {
    return this.api.getLoginHistory();
  }

  getAuditHistory(): Observable<AuditObj> {
    return this.api.getAuditHistory();
  }

  getLimits(): Observable<any> {
    return this.api.getLimits();
  }
}
