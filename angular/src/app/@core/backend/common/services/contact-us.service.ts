import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContactUsApi } from '../api/contact-us.api';
import { ContactUs, ContactUsData } from '../../../interfaces/common/contact-us';

@Injectable()
export class ContactUsService extends ContactUsData {

  constructor(private api: ContactUsApi) {
    super();
  }

  post(msgObj: any): Observable<any> {
    return this.api.post(msgObj);
  }
}
