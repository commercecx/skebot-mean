import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { ContactUs } from '../../../interfaces/common/contact-us';

@Injectable()
export class ContactUsApi {
  private readonly apiController: string = 'contactus';

  constructor(private api: HttpService) { }

  post(msgObj: any): Observable<any> {
    return this.api.post(`${this.apiController}`, msgObj)
      .pipe(map(data => {
        return { ...data };
      }));
  }
}
