import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class MetadataApi {
  private readonly apiController: string = 'metadata';

  constructor(private api: HttpService) { }

  get(): Observable<any> {
    return this.api.get(`${this.apiController}/describe`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  post(mdType): Observable<any> {
    return this.api.post(`${this.apiController}/getComponentlist`, { 'metadata': mdType })
      .pipe(map(data => {
        return { ...data };
      }));
  }

}
