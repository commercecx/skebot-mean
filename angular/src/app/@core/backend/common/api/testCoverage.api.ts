import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class TestCoverageApi {
  private readonly apiController: string = 'testCoverage';

  constructor(private api: HttpService) {}

  get(): Observable<any> {
    return this.api.get(`${this.apiController}`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

}
