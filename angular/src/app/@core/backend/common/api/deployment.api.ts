import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class DeployApi {
  private readonly apiController: string = 'metadata';

  constructor(private api: HttpService) { }

  post(packageName: string, isValidate: boolean): Observable<any> {
    return this.api.post(`${this.apiController}/deploy/${packageName}?validate=${isValidate}`, {})
      .pipe(map(data => {
        return { ...data };
      }));
  }
}
