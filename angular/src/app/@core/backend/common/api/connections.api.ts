import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class ConnectionsApi {
  private readonly apiController: string = 'auth/connections';

  constructor(private api: HttpService) { }

  get(): Observable<any> {
    return this.api.get(`${this.apiController}`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  delete(id): Observable<string> {
    return this.api.delete(`${this.apiController}/delete/${id}`);
  }
}
