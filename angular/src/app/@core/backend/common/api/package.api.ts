import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { Package } from '../../../interfaces/common/package';

@Injectable()
export class PackageApi {
  private readonly apiController: string = 'metadata';

  constructor(private api: HttpService) { }

  get(): Observable<Package> {
    return this.api.get(`${this.apiController}/packages`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  post(createPackageObj): Observable<Package> {
    return this.api.post(`${this.apiController}/package/new/${createPackageObj.packageName}`, createPackageObj.compList)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  delete(id): Observable<string> {
    return this.api.delete(`${this.apiController}/delete/${id}`);
  }
}
