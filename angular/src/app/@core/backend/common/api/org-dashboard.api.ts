import { LoginObj, AuditObj } from './../../../interfaces/common/org-dashboard';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class OrgDashboardApi {
  private readonly apiController: string = 'orghealth';

  constructor(private api: HttpService) { }

  getLoginHistory(): Observable<LoginObj> {
    return this.api.get(`${this.apiController}/loginhistory`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  getAuditHistory(): Observable<AuditObj> {
    return this.api.get(`${this.apiController}/setupAuditHistory`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

  getLimits(): Observable<any> {
    return this.api.get(`${this.apiController}/limits`)
      .pipe(map(data => {
        return { ...data };
      }));
  }

}
