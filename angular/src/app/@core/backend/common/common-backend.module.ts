import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserData } from '../../interfaces/common/users';
import { UsersService } from './services/users.service';
import { UsersApi } from './api/users.api';
import { TestCoverageData } from '../../interfaces/common/testCoverage';
import { TestCoverageService } from './services/testCoverage.service';
import { TestCoverageApi } from './api/testCoverage.api';
import { ConnectionsData } from '../../interfaces/common/connections';
import { ConnectionsService } from './services/connections.service';
import { ConnectionsApi } from './api/connections.api';
import { MetadataData } from '../../interfaces/common/metadata';
import { MetadataService } from './services/metadata.service';
import { MetadataApi } from './api/metadata.api';
import { PackageData } from '../../interfaces/common/package';
import { PackageService } from './services/package.service';
import { PackageApi } from './api/package.api';
import { DeployData } from '../../interfaces/common/deployment';
import { DeployService } from './services/deployment.service';
import { DeployApi } from './api/deployment.api';
import { OrgDashboardData } from '../../interfaces/common/org-dashboard';
import { OrgDashboardService } from './services/org-dashboard.service';
import { OrgDashboardApi } from './api/org-dashboard.api';
import { ContactUsData } from '../../interfaces/common/contact-us';
import { ContactUsService } from './services/contact-us.service';
import { ContactUsApi } from '../common/api/contact-us.api';
import { HttpService } from './api/http.service';
import { CountryData } from '../../interfaces/common/countries';
import { CountriesService } from './services/countries.service';
import { CountriesApi } from './api/countries.api';
import { SettingsApi } from './api/settings.api';
import { NbAuthModule } from '@nebular/auth';
import { SettingsData } from '../../interfaces/common/settings';
import { SettingsService } from './services/settings.service';


// tslint:disable-next-line:max-line-length
const API = [
  UsersApi, TestCoverageApi, ConnectionsApi,
  MetadataApi, PackageApi, DeployApi,
  OrgDashboardApi, ContactUsApi, CountriesApi,
  SettingsApi, HttpService,
];

const SERVICES = [
  { provide: UserData, useClass: UsersService },
  { provide: TestCoverageData, useClass: TestCoverageService },
  { provide: ConnectionsData, useClass: ConnectionsService },
  { provide: MetadataData, useClass: MetadataService },
  { provide: PackageData, useClass: PackageService },
  { provide: DeployData, useClass: DeployService },
  { provide: OrgDashboardData, useClass: OrgDashboardService },
  { provide: ContactUsData, useClass: ContactUsService },
  { provide: CountryData, useClass: CountriesService },
  { provide: SettingsData, useClass: SettingsService },
];

@NgModule({
  imports: [CommonModule, NbAuthModule],
})
export class CommonBackendModule {
  static forRoot(): ModuleWithProviders<CommonBackendModule> {
    return {
      ngModule: CommonBackendModule,
      providers: [
        ...API,
        ...SERVICES,
      ],
    };
  }
}
