import { Subject } from 'rxjs';
import { ActionTypes } from './actions';

interface InitialState {
  loginHistory: Array<Object>;
  auditHistory: Array<Object>;
  limitsList: Array<Object>;
  testCoverage: Array<Object>;
  testCoveragePercentage: number;
}

interface Event {
  type: String;
  payload?: any;
}

const state: InitialState = {
  loginHistory: [],
  auditHistory: [],
  limitsList: [],
  testCoverage: [],
  testCoveragePercentage: 0,
};

export const dashboardStore = new Subject<InitialState>();
export const dashboardEventDispatcher = new Subject<Event>();

dashboardEventDispatcher.subscribe((data: Event) => {
  switch (data.type) {
    case ActionTypes.GET_LOGINHISTORY:
    case ActionTypes.GET_AUDITHISTORY:
    case ActionTypes.GET_LIMITS:
    case ActionTypes.GET_TESTCOVERAGE:
    case ActionTypes.GET_TESTCOVERAGEPERCENTAGE:
      dashboardStore.next(state);
      break;

    case ActionTypes.SET_LOGINHISTORY:
      state.loginHistory = data.payload;
      dashboardStore.next(state);
      break;

    case ActionTypes.SET_AUDITHISTORY:
      state.auditHistory = data.payload;
      dashboardStore.next(state);
      break;

    case ActionTypes.SET_LIMITS:
      state.limitsList = data.payload;
      dashboardStore.next(state);
      break;

    case ActionTypes.SET_TESTCOVERAGE:
      state.testCoverage = data.payload;
      dashboardStore.next(state);
      break;

    case ActionTypes.SET_TESTCOVERAGEPERCENTAGE:
      state.testCoveragePercentage = data.payload;
      dashboardStore.next(state);
      break;

    default:
      break;
  }
});
