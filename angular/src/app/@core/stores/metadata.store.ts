import { MetadataData, Metadata } from '../interfaces/common/metadata';
import { PackageData } from '../interfaces/common/package';
import { Subject } from 'rxjs';
import { ActionTypes } from './actions';

interface InitialState {
  mdList: Array<Metadata>;
  packageList: Array<Metadata>;
}

interface Event {
  type: String;
  payload?: any;
}

const state: InitialState = {
  mdList: [],
  packageList: [],
};

export const metadataStore = new Subject<InitialState>();
export const metadataEventDispatcher = new Subject<Event>();

metadataEventDispatcher.subscribe((data: Event) => {
  switch (data.type) {
    case ActionTypes.GET_METADATA:
    case ActionTypes.GET_PACKAGES:
      metadataStore.next(state);
      break;

    case ActionTypes.SET_METADATA:
      state.mdList = data.payload;
      metadataStore.next(state);
      break;

    case ActionTypes.SET_PACKAGES:
      state.packageList = data.payload;
      metadataStore.next(state);
      break;

    // case ActionTypes.CREATE_PACKAGE:
    //   state.packageList.push(data.payload);
    //   metadataStore.next(state);
    //   break;

    default:
      break;
  }
});
