import { Connection, ConnectionsData } from './../interfaces/common/connections';
import { Subject } from 'rxjs';
import { ActionTypes } from './actions';

interface InitialState {
  connections: Array<Object>;
  activeConnection: string;
  destConnection: string;
}

interface Event {
  type: String;
  payload?: any;
}

const state: InitialState = {
  connections: [],
  activeConnection: '',
  destConnection: '',
};

export const connectionStore = new Subject<InitialState>();
export const connectionEventDispatcher = new Subject<Event>();

connectionEventDispatcher.subscribe((data: Event) => {
  switch (data.type) {
    case ActionTypes.GET_CONNECTIONS:
      connectionStore.next(state);
      break;

    case ActionTypes.SET_CONNECTIONS:
      state.connections = data.payload;
      connectionStore.next(state);
      break;

    case ActionTypes.CREATE_CONNECTION:
      state.connections = [...state.connections, data.payload];
      connectionStore.next(state);
      break;

    case ActionTypes.DELETE_CONNECTION:
      const { connections } = state;
      const id = data.payload;
      const updatedConnections = connections.filter((connection: Connection) => connection._id !== id);
      state.connections = updatedConnections;
      connectionStore.next(state);
      break;

    case ActionTypes.CHANGE_CONNECTION:
      state.activeConnection = data.payload;
      connectionStore.next(state);
      break;

      case ActionTypes.CHANGE_DEST_CONNECTION:
        state.destConnection = data.payload;
        connectionStore.next(state);
        break;

    default:
      break;
  }
});
