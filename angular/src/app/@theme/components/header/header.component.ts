import { ActionTypes } from './../../../@core/stores/actions';
import { connectionStore, connectionEventDispatcher } from './../../../@core/stores/connection.store';
import { metadataStore, metadataEventDispatcher } from './../../../@core/stores/metadata.store';
import { dashboardStore, dashboardEventDispatcher } from './../../../@core/stores/dashboard.store';
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { LayoutService } from '../../../@core/utils';
import { map, takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserStore } from '../../../@core/stores/user.store';
import { SettingsData } from '../../../@core/interfaces/common/settings';
import { User } from '../../../@core/interfaces/common/users';
import { ConnectionsData, Connection } from './../../../@core/interfaces/common/connections';
import { MetadataData, Metadata } from './../../../@core/interfaces/common/metadata';
import { OrgDashboardData, AuditObj, LoginObj } from './../../../@core/interfaces/common/org-dashboard';
import { PackageData, Package } from './../../../@core/interfaces/common/package';
import { TestCoverageData } from './../../../@core/interfaces/common/testCoverage';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: User;
  userName: String;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = this.getMenuItems();

  connList = [];

  selectedOrg: string = '';
  selectedOrgName: string = 'Change Connection';

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userStore: UserStore,
    private settingsService: SettingsData,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private connectionsService: ConnectionsData,
    private metadataService: MetadataData,
    private dashboardService: OrgDashboardData,
    private packageService: PackageData,
    private testCoverageService: TestCoverageData,
    private nbMenuService: NbMenuService,
  ) {
  }

  getMenuItems() {
    // const userLink = this.user ?  '/pages/users/current/' : '';
    const userName = this.user ? this.user.username : '';
    this.userName = userName;
    return [
      // { title: 'Profile', link: userLink, queryParams: { profile: true } },
      { title: userName },
      { title: 'Log out', link: '/auth/logout' },
    ];
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;
    this.userStore.onUserStateChange()
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe((user: User) => {
        this.user = user;
        this.userMenu = this.getMenuItems();
      });

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => {
        this.userPictureOnly = isLessThanXl;
      });

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => {
        this.currentTheme = themeName;
      });

    connectionStore.subscribe((state) => {
      this.connList = state.connections;
    });

    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'connection-menu'),
        map(({ item: item }) => item),
      )
      .subscribe(item => {
        if (this.selectedOrg !== item['_id']) {
          this.selectedOrgName = item['title'];
          this.selectedOrg = item['_id'];
          this.loadOrgSpecificData(false);
        }
      });

    this.loadOrgSpecificData(true);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.userStore.setSetting(themeName);
    this.settingsService.updateCurrent(this.userStore.getUser().settings)
      .pipe(takeUntil(this.destroy$))
      .subscribe();

    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  getConnections() {
    const tempData = [];
    const loadConnectionsData = this.connectionsService.get();
    const that = this;
    loadConnectionsData
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Connection) => {
        tempData.push({
          _id: '',
          name: 'LoggedIn Org',
          title: 'LoggedIn Org',
        });
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          data[keys[i]]['title'] = data[keys[i]]['name'];
          tempData.push(data[keys[i]]);
        }
        connectionEventDispatcher.next({ type: ActionTypes.SET_CONNECTIONS, payload: tempData });
      });
  }

  getMetadata() {
    const tempData = [];
    const loadMetadataData = this.metadataService.get();
    loadMetadataData
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Metadata) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          if (data[keys[i]].name !== 'CustomLabels') {
            data[keys[i]].components = [];
            data[keys[i]].indeterminate = false;
            data[keys[i]].isClicked = false;
            tempData.push(data[keys[i]]);
          }
        }
        tempData[0].isClicked = true;
        metadataEventDispatcher.next({ type: ActionTypes.SET_METADATA, payload: tempData });
      });
  }

  loadLoginHistory() {
    const tempData = [];
    this.dashboardService.getLoginHistory()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: LoginObj) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          if (data[keys[i]]['LoginGeo'] === null || data[keys[i]]['LoginGeo'] === undefined) {
            data[keys[i]]['LoginGeo'] = {
              'Country': 'Others',
            };
          } else {
            if (data[keys[i]]['LoginGeo'].Country === null || data[keys[i]]['LoginGeo'].Country === undefined)
              data[keys[i]]['Country'] = 'Others';
            else
              data[keys[i]]['Country'] = data[keys[i]]['LoginGeo'].Country;
          }
          tempData.push(data[keys[i]]);
        }
        dashboardEventDispatcher.next({ type: ActionTypes.SET_LOGINHISTORY, payload: tempData });
      });
  }

  loadAuditHistory() {
    const tempData = [];
    this.dashboardService.getAuditHistory()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: AuditObj) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          if (data[keys[i]]['CreatedBy'] === null || data[keys[i]]['CreatedBy'] === undefined) {
            data[keys[i]]['CreatedBy'] = 'Others';
          } else {
            if (data[keys[i]]['CreatedBy'].Username === null || data[keys[i]]['CreatedBy'].Username === undefined)
              data[keys[i]]['CreatedBy'] = 'Others';
            else
              data[keys[i]]['CreatedBy'] = data[keys[i]]['CreatedBy'].Username;
          }
          tempData.push(data[keys[i]]);
        }
        dashboardEventDispatcher.next({ type: ActionTypes.SET_AUDITHISTORY, payload: tempData });
      });
  }

  loadLimits() {
    const tempData = [];
    this.dashboardService.getLimits()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          if (keys[i] === 'DataStorageMB' || keys[i] === 'FileStorageMB' || keys[i] === 'DailyApiRequests') {
            tempData.push({
              'title': keys[i],
              'data': {
                labels: ['Remaining', 'Used'],
                datasets: [{
                  data: [data[keys[i]].Remaining, data[keys[i]].Max - data[keys[i]].Remaining],
                  backgroundColor: ['#48C9B0', '#EB984E'],
                }],
              },
              'options': {
                maintainAspectRatio: false,
                responsive: true,
                scales: {
                  xAxes: [
                    {
                      display: false,
                    },
                  ],
                  yAxes: [
                    {
                      display: false,
                    },
                  ],
                },
                legend: {
                  labels: {
                    fontColor: '#222',
                  },
                },
              },
            });
          }
        }
        dashboardEventDispatcher.next({ type: ActionTypes.SET_LIMITS, payload: tempData });
      });
  }

  loadPackages() {
    const tempData = [];
    this.packageService.get()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Package) => {
        const keys = Object.keys(data);
        for (let i = 0; i < keys.length; i++) {
          tempData.push(data[keys[i]]);
        }
        metadataEventDispatcher.next({ type: ActionTypes.SET_PACKAGES, payload: tempData });
      });
  }

  loadTestCoverage() {
    const tempData = [];
    this.testCoverageService.get()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => {
        let orgTotalLinesCovered: number = 0;
        let orgTotalLines: number = 0;
        for (const d of Object.values(data)) {
          d.LastModifiedBy = d.LastModifiedBy.Name;
          d.ApexClassOrTrigger = d.ApexClassOrTrigger.Name;
          d.TotalNumberOfLines = d.NumLinesCovered + d.NumLinesUncovered;
          d.TotalPercentage = d.TotalNumberOfLines > 0 ? (d.NumLinesCovered / d.TotalNumberOfLines) * 100 : 0;
          orgTotalLinesCovered = orgTotalLinesCovered + d.NumLinesCovered;
          orgTotalLines = orgTotalLines + d.TotalNumberOfLines;
          tempData.push(d);
        }
        dashboardEventDispatcher.next({ type: ActionTypes.SET_TESTCOVERAGE, payload: tempData });
        dashboardEventDispatcher.next({
          type: ActionTypes.SET_TESTCOVERAGEPERCENTAGE,
          payload: (orgTotalLinesCovered / orgTotalLines),
        });
      });
  }

  loadOrgSpecificData(includeConnections) {
    if (includeConnections) {
      this.getConnections();
    } else {
      connectionEventDispatcher.next({ type: ActionTypes.CHANGE_CONNECTION, payload: this.selectedOrg });
    }
    this.getMetadata();
    this.loadLoginHistory();
    this.loadAuditHistory();
    this.loadLimits();
    this.loadPackages();
    this.loadTestCoverage();
  }
}
