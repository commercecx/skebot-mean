import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"><b>
    © {{ currentYear }}
    <a href="https://commercecx.com/" target="_blank" rel="noopener">CommcerceCX  </a></b> </span>
    <div class="socials">
      <a href="shared/contact-us">
        <nb-icon icon="email-outline"></nb-icon>
        <label>
          Contact Us
        </label>
      </a>
      <a href="https://twitter.com/commercecx" target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://www.linkedin.com/company/commerce-cx/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
  get currentYear(): number {
    return new Date().getFullYear();
  }
}
