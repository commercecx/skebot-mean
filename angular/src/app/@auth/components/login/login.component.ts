import { ChangeDetectionStrategy, Component, OnInit, TemplateRef } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxLoginComponent implements OnInit {

  acceptTermCondition: boolean;
  selectedEnvironement: string;

  submitted: boolean = false;

  constructor(private dialogService: NbDialogService) { }

  ngOnInit(): void {

  }

  login(): void {
    window.location.href = `${environment.apiUrl}/auth/login/${this.selectedEnvironement}`;
  }

  getConfigValue(key: string): any {
    // return getDeepFromObject(this.options, key, null);
  }

  toggleAcceptTermsCondition(checked: boolean) {
    this.acceptTermCondition = checked;
  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { context: '<b>this is some additional data passed to dialog</b>' });
  }
}
