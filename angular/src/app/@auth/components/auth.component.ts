import { Component, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';

import { NbAuthService } from '@nebular/auth';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'ngx-auth',
  styleUrls: ['./auth.component.scss'],
  template: `
    <nb-layout>
      <nb-layout-header><b style="font-size: 1.75rem">Skebot</b></nb-layout-header>
      <nb-layout-column class="colored-column-danger">
        <nb-auth-block>
        <router-outlet></router-outlet>
        </nb-auth-block>
      </nb-layout-column>
      <nb-layout-footer>
        <div class="footer">
          <div>
            <span class="created-by">
              <b>© {{ currentYear }}
                <a href="https://commercecx.com/" target="_blank" rel="noopener">CommcerceCX  </a>
              </b>
            </span>
          </div>
          <div class="socials">
            <a href="shared/contact-us" title="Contact Us" style="text-decoration:none;">
              <nb-icon icon="email-outline"></nb-icon>
              <span class="pl-1">Contact Us</span>
            </a>
            <a href="https://twitter.com/commercecx" target="_blank" title="Twitter" class="ion ion-social-twitter"></a>
            <a href="https://www.linkedin.com/company/commerce-cx/" title="LinkedIn" target="_blank" class="ion ion-social-linkedin"></a>
          </div>
        </div>
      </nb-layout-footer>
    </nb-layout>
  `,
})
export class NgxAuthComponent implements OnDestroy {

  private alive = true;

  subscription: any;

  authenticated: boolean = false;
  token: string = '';

  // showcase of how to use the onAuthenticationChange method
  constructor(protected auth: NbAuthService, protected location: Location) {

    this.subscription = auth.onAuthenticationChange()
      .pipe(takeWhile(() => this.alive))
      .subscribe((authenticated: boolean) => {
        this.authenticated = authenticated;
      });
  }

  back() {
    this.location.back();
    return false;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  get currentYear(): number {
    return new Date().getFullYear();
  }
}
