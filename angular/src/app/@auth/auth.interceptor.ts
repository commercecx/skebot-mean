import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { ConnectionsData } from './../@core/interfaces/common/connections';
import { connectionStore, connectionEventDispatcher } from './../@core/stores/connection.store';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private connectionsService: ConnectionsData,
  ) {
    connectionStore.subscribe((state) => {
      this.activeConnection = state.activeConnection;
      this.destConnection = state.destConnection;
    });
  }

  activeConnection: string = '';
  destConnection: string = '';

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = {
      Authorization: `Bearer ${this.authService.getTokenString()}`,
    };
    if (this.activeConnection !== '')
      headers['Source'] = this.activeConnection;
    if (this.destConnection !== '')
      headers['Destination'] = this.destConnection;
    req = req.clone({
      setHeaders: headers,
    });
    return next.handle(req)
      .pipe(catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.authService.logout();
          this.router.navigate(['auth/login']);
        }
        // TODO: handle 403 error ?
        return throwError(error);
      }));
  }
}
