import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

@Pipe({ name: 'ngxAuthToken' })
export class AuthPipe implements PipeTransform {
  constructor(
    // private authService: NbAuthService,
    private cAuthService: AuthenticationService) { }

  transform(url: string): Observable<string> {
    if (!url) {
      return observableOf(url);
    }
    return this.cAuthService.getToken()
      .pipe(map((token) => {
        const separator = url.indexOf('?') > 0 ? '&' : '?';
        return `${url}${separator}token=${token}`;
      }));

  }
}
