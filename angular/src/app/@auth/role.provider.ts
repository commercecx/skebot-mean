import { NbRoleProvider } from '@nebular/security';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class RoleProvider extends NbRoleProvider {

  constructor(
    // private authService: NbAuthService,
    private cauthService: AuthenticationService) {
    super();
  }

  getLowerCaseRoles(roles: any): string | string[] {
    if (Array.isArray(roles)) {
      roles = roles.map(element => {
        return element.toLowerCase();
      });
    } else {
      roles = roles.toLowerCase();
    }
    return roles;
  }

  getRole(): Observable<string | string[]> {
    return this.cauthService.getToken()
      .pipe(
        /* map((token) => {
          console.log(token);
          //const payload = token.getAccessTokenPayload();
          //var cond = token.isValid() && payload && payload['role'];
          //return !!(cond) ? this.getLowerCaseRoles(payload['role']) : 'guest';
          //console.log(payload);
          return 'admin';
        }), */
        tap(token => {
          // console.log(token)
          // TODO : Return valid role
          return 'admin';
        }),
      );
  }
}
