export const environment = {
  production: false,
  apiUrl: 'https://stage.skebot.com/api',
  testUser: {
    token: {},
    email: '',
  },
};

// ng build --prod
// ng build --configuration=staging