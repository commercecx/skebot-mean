module.exports = errorHandler;

function errorHandler(err, req, res, next) {

    if (typeof (err) === 'string' || err.isCustom) {
        // custom application error
        return res.status(400).json({ message: err.message });
    }

    if (err.name === 'ValidationError') {
        // mongoose validation error
        return res.status(400).json({ message: err.message });
    }

    if (err.name === 'UnauthorizedError' || (err.message && err.message.includes('expired access/refresh token'))) {
        // jwt authentication error
        return res.status(401).send( 'Unauthorized request' );
    }

    // default to 500 server error
    return res.status(500).send( err.message );
}