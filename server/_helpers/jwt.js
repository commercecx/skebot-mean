const expressJwt = require('express-jwt'); 
const authService = require('../authentication/authService');
const pathToRegexp = require('path-to-regexp');
module.exports = jwt;

function jwt() {
    const secret = process.env.JWT_SECRET;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/api/auth/authenticate',
            '/api/users/current',
            '/api/auth/register',
            '/api/auth/callback',        
            '/api/auth/login/prod',
            '/api/contactus',
            '/api/auth/login/sandbox',        
            '/api/auth/login/test',        
            /^\/api\/auth\/callback\/.*/,
            /^\/api\/users\/.*\/photo\/?.*/,
            /^\/api\/auth\/connection\/login\/.*/,         
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await authService.getBySFUserId(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};