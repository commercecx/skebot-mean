const mongoose = require('mongoose');
const connectionOptions = { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false };
mongoose.connect(process.env.MONGODB_URI, connectionOptions, ()=>{console.log('Database connection succesfull')});
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../authentication/user.model')
};
