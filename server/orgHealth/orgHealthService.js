module.exports = {
    getloginHistory,
    getSetupAuditHistory,
    getorgLimits
};

async function getloginHistory(sfConn) { 
    var records = [];
    try{
        await sfConn.query(`select id,ApiType, Browser,Status, LoginGeo.Country, Application, LoginTime, LoginType  from LoginHistory order by LoginTime desc`,function(err, results){
            if (err) { error = err.errorCode; return;}        
            records = results.records;		
        });	
    }catch(e){ throw new Error(e); }
			
    return records;
}


async function getSetupAuditHistory(sfConn) { 
    var records = [];
    try{
        await sfConn.query(`select id,CreatedDate, CreatedBy.Username, DelegateUser, Action, Display from SetupAuditTrail order by CreatedDate desc`,function(err, results){
            if (err) { error = err.errorCode; return;}        
            records = results.records;		
        });	
    }catch(e){ throw new Error(e); }
			
    return records;
}

async function getorgLimits(sfConn) { 
    var records = {};
    try{
        if(!sfConn.accessToken){             
            const getRefreshToken = () => {
                return new Promise((resolve, reject) => {
                    sfConn.query(`select id from user limit 1`, (err, results) => {
                        if (err) return reject(err);
                        resolve(results);
                      });                   
                })
            }
            await getRefreshToken();
        }       
        let endPointurl = sfConn.instanceUrl+ `/services/data/v${process.env.API_VERSION}/limits/`;
        let request = require("request"); 
        const options = {
            url: endPointurl,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer '+sfConn.accessToken
            }
        };
        const getLimits = () => {
            return new Promise((resolve, reject) => {
                request(options, function(err, res, body) {
                    if (err) { console.error(err); reject(err);}
                     resolve(JSON.parse(body));
                });                  
            })
        }
        records =  await getLimits();
    }catch(e){ throw new Error(e); }
			
    return records;
}
