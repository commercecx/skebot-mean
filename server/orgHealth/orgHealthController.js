var jsforce = require('jsforce');
const ogHealthService = require('./orgHealthService');
const request = require('request');
const jwt = require('jsonwebtoken');
const authService = require('../authentication/authService');
const fs = require('fs');


module.exports = {
    getloginHistory ,
    getSetupAuditHistory ,
    getOrgLimits  ,
    download,
    upload
};

async function getloginHistory(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        ogHealthService.getloginHistory(connections.source.sfConn)
        .then(coverageData => {
             if(coverageData && coverageData.includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
             }
             res.json(coverageData)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}


async function getSetupAuditHistory(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        ogHealthService.getSetupAuditHistory(connections.source.sfConn)
        .then(coverageData => {
             if(coverageData && coverageData.includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
             }
             res.json(coverageData)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}


async function getOrgLimits(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        ogHealthService.getorgLimits(connections.source.sfConn)
        .then(limits => {
             if(limits && limits.toString().includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
             }
             res.json(limits)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}



async function download(req, res, next) {
    var readStream = fs.createReadStream('./source.txt');        

    var readStream = fs.createReadStream('./CertificationsCSV.csv');

    res.set('Content-disposition', 'attachment; filename=' + 'fileName.csv');
    res.set('Content-Type', 'text/plain');
    //res.setHeader('Content-Transfer-Encoding', 'binary');
	//res.setHeader('Content-Type', 'application/octet-stream');    
  
    readStream.pipe(res);    
}

async function upload(req, res, next) {
    const file = req.file;
    console.log(file.filename);
    if (!file) {
      const error = new Error('No File')
      error.httpStatusCode = 400
      return next(error)
    }
      res.send(file);
}

