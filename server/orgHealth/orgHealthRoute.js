var express = require('express');
var router = express.Router();
var authorize = require('../_helpers/authorize');
const orgHealthController = require('./orgHealthController');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'uploads')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  }) 
const upload = multer({ storage: storage });

// Authorized Routes
//router.get('/current', userController.getCurrent);
router.get('/loginhistory', authorize(), orgHealthController.getloginHistory);
router.get('/setupAuditHistory', authorize(), orgHealthController.getSetupAuditHistory);
router.get('/limits', authorize(), orgHealthController.getOrgLimits);
router.get('/download', orgHealthController.download);
router.post('/upload/data', [authorize(), upload.single('file')] , orgHealthController.upload);


module.exports = router;