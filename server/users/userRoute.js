var express = require('express');
var router = express.Router();
var Role = require('../_helpers/role');
var authorize = require('../_helpers/authorize');
var userController = require('./userController');

// Authorized Routes
//router.get('/current', userController.getCurrent);
router.get('/current', authorize(), userController.getCurrent);
router.get('/:id',authorize(), userController.getById);
router.get('/:id/photo',userController.getPhoto);

// Admin Authorized Routes
router.get('/',authorize(Role.Admin), userController.getAll);
router.put('/:id', authorize(Role.Admin), userController.update);
router.delete('/:id', authorize(Role.Admin), userController.delete);

module.exports = router;