var express = require('express');
var router = express.Router();

router.use('/auth', require('../authentication/authRoute.js'));
router.use('/users', require('../users/userRoute'));
router.use('/contactus', require('../contactus/contactUSRoute'));
router.use('/testCoverage', require('../testCoverage/testCoverageRoute'));
router.use('/metadata', require('../metadata/metadataRoute'));
router.use('/orghealth', require('../orgHealth/orgHealthRoute'));

module.exports = router;