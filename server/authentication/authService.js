const jsforce = require('jsforce');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const Role = require('../_helpers/role');
const User = db.User;
const encript = require('../_helpers/encryption').encrypt;
const decrypt = require('../_helpers/encryption').decrypt;


module.exports = {
    authenticate,
    authorizeCode, authorizeConnection,
    getConnectionById, deleteConnection : _deleteConnection,
    newSFConnection,
    getAll,
    getById,
    getConnections,
    getBySFUserId,
    create,
    update,
    delete: _delete
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const token = jwt.sign({ sub: user.id }, process.env.JWT_SECRET);
        return {
            ...user.toJSON(),
            token
        };
    }
}

async function authorizeCode(code, env) {  
    let url = env === "prod" ? "https://login.salesforce.com":  "https://test.salesforce.com";     
    const oauth2 = new jsforce.OAuth2({     
        loginUrl :   url, 
        clientId : process.env.CLIENT_ID,
        clientSecret : process.env.CLIENT_SECRET,
        redirectUri : process.env.REDIRECT_URI
    });	
    const sfConn = new jsforce.Connection({ oauth2 : oauth2 });
    var sfUser;
    try{
        await sfConn.authorize(code ,  async function(err, userInfo) {
            if (err) {  return err;/*throw new Error('Error in authorizing Code'); */ }
            // Now you can get the access token, refresh token, and instance URL information.
            // Save them to establish connection next time.  
            if(userInfo && sfConn && sfConn.accessToken){
                sfUser = userInfo ;
                sfUser.instanceUrl = sfConn.instanceUrl; 
            }
        });
    } catch(err){
        throw new Error(err);
    }
    
    if(sfUser){
        await sfConn.query("select id,firstname,SmallPhotoUrl, lastname, username, email,MobilePhone,Phone, CompanyName  from user where id = '"+sfUser.id+"'",async function(err, res) {
            if (err) { throw new Error(err);  }
            if(res && res.records){
                sfUser.email = res.records[0].Email;
                sfUser.username = res.records[0].Username;
                sfUser.firstName = res.records[0].FirstName;
                sfUser.lastName = res.records[0].LastName;
                sfUser.companyName = res.records[0].CompanyName;
                sfUser.smallPhotoUrl = res.records[0].SmallPhotoUrl;  
                sfUser.mobilePhone = res.records[0].MobilePhone;
                sfUser.phone = res.records[0].Phone;                                                
            }else{
                return new Error('Error in Getting userInfo');  
            }
        });
        try{
            // Check user in DB with UserId
            const user = await User.findOne({ id: sfUser.id });
            if(user === null){
                const user = new User(sfUser);
                await user.save();
            }else{
                // Update last Login 
                Object.assign(user, {...sfUser, lastLoginDate: Date.now() });
                await user.save();
            }
            var role = Role.User;
            if(user && user.companName == 'Css'){
                role = Role.Admin;
            }
            const token = jwt.sign({ sub: sfUser.id, at: sfConn.accessToken ,domain: sfConn.instanceUrl , role: role}, process.env.JWT_SECRET);
            sfUser.token = token;
        }catch(e){
            throw new Error('Error in creating/Updating user'+ e);
        }
    }
    let loggedInUser = {
        "username": sfUser.username,
        "token": sfUser.token,
        "firstName": sfUser.firstName,
        "lastName": sfUser.lastName,
    }
    return loggedInUser;
}


async function authorizeConnection(currentUserId, code, orgType, connectionName) {  
    let url = orgType == "prod"? "https://login.salesforce.com": "https://test.salesforce.com";  
    const oauth2 = new jsforce.OAuth2({
        loginUrl : url,
        clientId : process.env.CLIENT_ID,
        clientSecret : process.env.CLIENT_SECRET,
        redirectUri : process.env.CONN_REDIRECT_URI
    });	
    const sfConn = new jsforce.Connection({ oauth2 : oauth2 });
    var connection = {};
    try{
        await sfConn.authorize(code ,  async function(err, userInfo) {
            if (err) {  return err; }
            connection.refreshToken = encript(sfConn.refreshToken);
            connection.instanceUrl = sfConn.instanceUrl;
            connection.userId = userInfo.id;
            connection.organizationId = userInfo.organizationId;
            connection.name = connectionName;
            connection.orgType = orgType;
        });
    } catch(err){
        throw new Error(err);
    }  
    
    if(connection){
         await sfConn.query("select id,firstname, lastname, username, email,mobilePhone,Phone, CompanyName  from user where id = '"+connection.userId+"'",async function(err, res) {
            if (err) { return err;  }
            if(res && res.records){
                connection.email = res.records[0].Email;
                connection.username = res.records[0].Username;
                connection.firstName = res.records[0].FirstName;
                connection.lastName = res.records[0].Lastname;
                connection.companyName = res.records[0].CompanyName; 
                connection.mobilePhone = res.records[0].MobilePhone;
                connection.phone = res.records[0].Phone;                                
            }else{
                return new Error('Error in Getting userInfo');  
            }
        }); 
        try{          
            // Check user in DB with UserId
            const user = await User.findOne({ id: currentUserId });
            if(user != null){
                // Update user Connection Login 
                user.connections.push(connection);
                await user.save();
            }
        }catch(e){
            throw new Error('Error in creating/Updating user'+ e)
        }
    }
    return connection;
}


function getConnectionById(connections, connectionId) {
    if(connections){
        let connection;
        connections.map(value => { 
            if(connectionId.toString().includes(value._id)){
                connection =  value;
                if(connection.refreshToken){
                    connection.refreshToken = decrypt(connection.refreshToken);
                }                
            } 
        });
        return connection;
    }else{ return; }
}

async function _deleteConnection(currentUserId, connectionid) {    
    const user = await User.findOne({ id: currentUserId });
    user.connections.id(connectionid).remove();
    await user.save();
}

async function getAll() {
    return await User.find();
}

async function getById(id) {
    return await User.findById(id);
}

async function getBySFUserId(id) {
    return await User.findOne({ id: id });
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);
    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}


async function newSFConnection(domain, accessToken,  refreshToken){
    return new jsforce.Connection({        
        oauth2 : {
            loginUrl: `${domain}`,
            clientId : process.env.CLIENT_ID,
            clientSecret : process.env.CLIENT_SECRET,
            redirectUri : process.env.REDIRECT_URI
        },
        accessToken : `${accessToken}`,
        instanceUrl : `${domain}`,        
        refreshToken : `${refreshToken}`
    });
}




async function getConnections(req, res){
    let sourceconnectionId = req.headers.source;
    let destinationconnectionId = req.headers.destination;
    // Salesforce Connections
    let defaultConncetion = await newSFConnection(req.user.domain,req.user.at);
    let sourceSfConnection, sConection, destinationSfConnection, dConection;
    if(sourceconnectionId || destinationconnectionId){
        let user =  await getBySFUserId(req.user.sub);        
        if(user && user.connections){
            if(sourceconnectionId){
                sConection = await getConnectionById(user.connections, sourceconnectionId);
                if(sConection){
                    sourceSfConnection = await newSFConnection(sConection.instanceUrl,'', sConection.refreshToken);
                }else{
                    return res.status(500).send(`No Connection found with Id ${sourceconnectionId}`);
                } 
            }
            if(destinationconnectionId){
                dConection = await getConnectionById(user.connections, destinationconnectionId);
                if(dConection){
                    destinationSfConnection = await newSFConnection(dConection.instanceUrl,'', dConection.refreshToken);
                }else{
                    return res.status(500).send(`No Connection found with Id ${destinationconnectionId}`);
                } 
            }        
        } else {
            return res.status(500).send('No Connection found for Current User');
        }      
    }
    let sourcePackageConnection = sourceSfConnection ? { "source": {"sfConn":sourceSfConnection, "userId": sConection.userId}} : { "source": {"sfConn":defaultConncetion, "userId": req.user.sub}};
    let destinationPackageConnection = destinationSfConnection ? { "destination": {"sfConn":destinationSfConnection, "userId": dConection.userId}} : null;
    
    return {...sourcePackageConnection,...destinationPackageConnection };
}