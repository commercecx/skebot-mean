const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var connectionSchema = new Schema({ 
    name: {type: 'string',  required: true} , 
    orgType: {type: 'string',  required: true} , 
    refreshToken: {type: 'string',  required: true}  ,
    username: { type: String,  required: true },
    email: { type: String, required: true },
    userId: { type: String, required: true },
    organizationId: { type: String, required: true },
    instanceUrl: { type: String, required: true },
    firstName: { type: String },
    lastName: { type: String },
    mobilePhone: { type: String },
    phone: { type: String },
    companyName: { type: String },
    createdDate: { type: Date, default: Date.now },
    lastLoginDate: { type: Date, default: Date.now }      
});

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    email: { type: String, required: true },
    id: { type: String, unique: true, required: true },
    organizationId: { type: String, required: true },
    instanceUrl: { type: String, required: true },
    hash: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    mobilePhone: { type: String },
    phone: { type: String },    
    companyName: { type: String },
    createdDate: { type: Date, default: Date.now },
    lastLoginDate: { type: Date, default: Date.now },
    smallPhotoUrl: { type: String },
    connections: [connectionSchema]
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.hash;
    }
});

module.exports = mongoose.model('User', schema);