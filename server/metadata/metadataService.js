var fs = require('fs');
const { stream } = require('stream');
const util = require('util');
const metadataHelper = require('./metadataHelper.js');

module.exports = {
    describe,
    retrieveComponentList,
    retrieveCreatedPackages,
    createdPackage,
    deployPackage
};

async function describe(sfConn) {    
    var data;
    var error;
    try{
        await sfConn.metadata.describe(`${process.env.API_VERSION}`, function(err, results) {	
            if (err) { error = err.errorCode; return;}  
            var arry = metadataHelper.formartDescribeResults(results);
            data = 	arry;
        });	
    }catch(e){ throw new Error(e); }
    return data;
}

async function retrieveComponentList(sfConn, reqBody) {
    let ListMetadataQuery = metadataHelper.getMetadataQuery(reqBody);       
    var data;
    var error;
    try{
        await sfConn.metadata.list(ListMetadataQuery,`${process.env.API_VERSION}`, function(err, results) {		
            if (err) { error = err.errorCode; return;}
            var listMetadata = [];
            if(results){
                data = results;
            }	
        });
    }catch(e){ throw new Error(e); }	   

    return data; 
}

async function retrieveCreatedPackages(sfConn, currentUserId) {
    let data = [], error;
    try{        
        await sfConn.query(`select id,title,tagcsv,createdby.name,VersionNumber,createddate,versiondata from contentVersion where createdbyId='${currentUserId}' order by createdDate desc`, function(err, result) {
            if (err) { return console.error(err); error = err; }                              
            if(result && result.records){                               
                for(let i=0; i<result.records.length; i++){                 
                    if(result.records[i].TagCsv && result.records[i].TagCsv.includes('Skebot')){
                        data.push(result.records[i]);
                    }
                } 
            }
        });	         	             
    }catch(e){ throw new Error(e);}	      
    return data;    
}

async function createdPackage(sfConn, packageName, reqBody) {
    if(sfConn && sfConn.metadata){
        sfConn.metadata.pollTimeout = 1200000;    
        let data = [], error;
        //var type = [{members:'Account', name:'CustomObject'},{members:'Contact', name:'CustomObject'}];
        types = [{"types" : reqBody}];
        try{	                
            let readZip = sfConn.metadata.retrieve({ unpackaged: types,apiVersion: `${process.env.API_VERSION}`}).stream();   
            let zipData = [];    
            let readZipFile = function(zipStream){
                return new Promise((resolve, reject) => {
                    zipStream.on("data", chunk => zipData.push(chunk));
                    zipStream.on("end", () => resolve(zipData));
                    zipStream.on("error", error => reject(error));
                });     
            }
            // Wait for read Zip
            let bufData = await readZipFile(readZip);
            // Concat buff Data got from Chunk
            let buf = Buffer.concat(bufData);
            // Convert buff data to base64
            let zData = buf.toString('base64');
            // Create Content record
            data = await createContentRecord(sfConn, packageName, zData);                  
        }catch(e){ throw new Error(e); }	   

        return data; 
         
    }
} 

async function createContentRecord(sfConn, packageName, zipData){
    let res;
	await sfConn.sobject("ContentVersion").create({ 
        title : `${packageName}`,
        versionData : zipData,
        pathOnClient : '/foo.zip',
        TagCsv : 'Skebot'
        }, function(err, ret) {
        if (err || !ret.success) { res = err; throw new Error(e); console.error(err, ret); }
        res = ret;
    });	
    return res;
}



async function deployPackage(ssfConn, dSfConn, packageId, validate) {
    if(ssfConn && ssfConn.metadata){
        ssfConn.metadata.pollTimeout = 1200000;
        dSfConn.metadata.pollTimeout = 1200000;    
        let data = [], error;
        try{	                     
            validate = (validate === 'false') ? false: true;             
            
            const getContentVersionId = () => {
                return new Promise((resolve, reject) => {
                    ssfConn.sobject("ContentVersion").retrieve(`${packageId}`, async function(err, record) {
                        if (err) { console.error(err); reject(err);}
                        resolve(record);
                    });
                })
            }

            let packageVerion = await getContentVersionId();            
            if(packageVerion && packageVerion.VersionData){
                var endPointurl = ssfConn.instanceUrl+packageVerion.VersionData;
                var options = {
                        headers: {
                          'Authorization': 'Bearer '+ssfConn.accessToken
                        },
                        method: "GET",
                        timeout: 12000000,
                        followRedirect: true,
                        maxRedirects: 10	
                };

                let fetch = require("node-fetch");
                let readPacakge = await fetch(endPointurl, options);                  
                let deployPacakge = dSfConn.metadata.deploy(readPacakge.body, { runTests: [ ],checkOnly: `${validate}`,rollbackOnError: true });
                
                let waitForDeployment = function(deployPacakge){
                    return new Promise((resolve, reject) => {
                        deployPacakge.complete((err, results) => {
                            if(err){
                                reject(err);
                            }                                
                            resolve(results);
                        });
                    });     
                }
                data = await waitForDeployment(deployPacakge);                 
            }else{
                error = packageVerion; 
            }            
        }catch(e){ throw new Error(e);}	

        return data;        
    }else{
        throw new Error('Error : Source Connection is not Defined');
    }
} 