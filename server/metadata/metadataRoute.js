var express = require('express');
var router = express.Router();
var authorize = require('../_helpers/authorize');
const metadataController = require('./metadataController');
// Authorized Routes
//router.get('/current', userController.getCurrent);
router.get('/describe', authorize(), metadataController.describe);
router.post('/getComponentlist', authorize(), metadataController.retrieveComponentList);

router.get('/packages', authorize(), metadataController.retrieveCreatedPackages);
router.post('/package/new/:packageName', authorize(), metadataController.createdPackage);

router.post('/deploy/:packageId', authorize(), metadataController.deployPackage);

module.exports = router;   