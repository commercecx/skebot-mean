let formartDescribeResults = function(results){
	var metadatArray = [];
	if(results){
		for(var i = 0;i < results.metadataObjects.length;i++){
			var xmlName = results.metadataObjects[i].xmlName;
			var directoryName = results.metadataObjects[i].directoryName;
			var childXmlNames = results.metadataObjects[i].childXmlNames;
			var childTags = '';
			if(childXmlNames){
				if(childXmlNames && childXmlNames.length > 0){		
					for(var y = 0;y < childXmlNames.length;y++){
						if(childTags == ''){
							childTags = childXmlNames[y];
							metadatArray.push({selected:false,name:childXmlNames[y],directoryName:directoryName});
						}
					}			
				}				
			}
			metadatArray.push({selected:false,name:xmlName,directoryName:directoryName});
		}
		return metadatArray;
	}	
}

let getMetadataQuery = function(objArray){
	var ListMetadataQuery = [];		
	for(var i=0;i<objArray.metadata.length;i++){		
		var MetadataQuery = {type:objArray.metadata[i],folder:""};
		ListMetadataQuery.push(MetadataQuery);	
	}	
	return ListMetadataQuery;
}

module.exports = {
	formartDescribeResults,
	getMetadataQuery
};