var jsforce = require('jsforce');
const metadataService = require('./metadataService');
const request = require('request');
const jwt = require('jsonwebtoken');
const authService = require('../authentication/authService');

module.exports = {
    describe  ,
    retrieveComponentList ,
    retrieveCreatedPackages ,
    createdPackage,
    deployPackage
};

async function describe(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        metadataService.describe(connections.source.sfConn)
        .then(data => {
            if(data && data.toString().includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
            }
            res.json(data)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}

async function retrieveComponentList(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        metadataService.retrieveComponentList(connections.source.sfConn,  req.body)
        .then(data => {
                if(data && data.toString().includes('INVALID_SESSION_ID')){
                    res.status(401).send('Session Expired');
                }
                res.json(data)
        })
        .catch(err => {if(err) {
            err.isCustom = true;
            let excption = err.toString();
            if(typeof (excption) === 'string' && excption.includes('INVALID_TYPE')){
                err.message = 'Invalid Input';
            }else{
                err.message = 'Unknon error';
            }
            next(err)
        } });
    }    
}

// Get All Created Packages 
async function retrieveCreatedPackages(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        metadataService.retrieveCreatedPackages(connections.source.sfConn, connections.source.userId)
        .then(data => {
            if(data && data.toString().includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
            }
            res.json(data)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}

// Created Packages
async function createdPackage(req, res, next) {
    let packageName = req.params.packageName;
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        metadataService.createdPackage(connections.source.sfConn , packageName,  req.body)
        .then(data => {
                if(data && data.toString().includes('INVALID_SESSION_ID')){
                    res.status(401).send('Session Expired');
                }
                res.json(data)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});  
    }
}

// Deploy Packages
async function deployPackage(req, res, next) {
    let packageId = req.params.packageId;
    let validate = req.query.validate;    
    let connections = await authService.getConnections(req, res);
    let data = [];
    if(connections && connections.source && connections.destination){
        metadataService.deployPackage(connections.source.sfConn ,connections.destination.sfConn , packageId, validate)
        .then(data => {
            if(data && data.toString().includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
            }
            res.json(data)
        })
        .catch(err => {if(err) { err.isCustom = true; console.log(err); } next(err)});          
    }else{
        res.status(500).json('Source and Destinaiton connection must be specified in Headers')
    }
}