var express = require('express');
var router = express.Router();
const contactUSController = require('./contactUSController');

// Authorized Routes
//router.get('/current', userController.getCurrent);
router.post('/',  contactUSController.sendEmail);


module.exports = router;