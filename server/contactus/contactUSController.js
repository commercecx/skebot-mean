
const sgMail = require('@sendgrid/mail');

module.exports = {
    sendEmail    
};

function sendEmail(req, res, next) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    console.log(req.body);
    const msg = {
        to:  process.env.CONTACTUS_TOEMAIL,				//receiver's email
        from: process.env.CONTACTUS_FROMEMAIL,			//sender's email
        subject: process.env.CONTACTUS_SUBJECT,			//Subject
        //text: String.valueOf(req.body) != "" ? String.valueOf(req.body): 'Message BLOCK',		                            //content
        html:  'From : <br>'+'<b>Name:</b> '+req.body.name+'<br><b>Email:</b> '+req.body.email+'<br><br> <b>Message: </b>'+req.body.message,			//HTML content
    };            
    (async () => {
        try {
            await sgMail.send(msg);
            res.json({"success":true, "message": "Email Sent Succesfully"});
        } catch (error) {    
            next(error);    
            if (error.response) {
                console.error(error.response.body);
                next(error.response.body);
            }
        }
    })();        
}

