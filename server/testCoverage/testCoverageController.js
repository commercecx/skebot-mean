var jsforce = require('jsforce');
const testCoverageService = require('./testCoverageService');
const request = require('request');
const jwt = require('jsonwebtoken');
const authService = require('../authentication/authService');

module.exports = {
    getAll    
};

async function getAll(req, res, next) {
    let connections = await authService.getConnections(req, res);
    if(connections && connections.source){
        testCoverageService.getAll(connections.source.sfConn)
        .then(coverageData => {
             if(coverageData && coverageData.includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
             }
             res.json(coverageData)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
    }    
}

