const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3001
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');


require('dotenv-flow').config({
  default_node_env: 'development'
});
const cors = require('cors'); 
const errorHandler = require('./server/_helpers/error-handler');

const app = express();

app.disable('x-powered-by');
const corsOptions = {
  origin: process.env.DOMAIN_URI,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(express.static(path.join(__dirname, `${process.env.AGDIST_PATH}`)))
app.use(cors(corsOptions));

//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.raw({ 
    type: 'application/octetstream',
    limit: '10mb'
}));


//app.use(cookieParser());
const jwt = require('./server/_helpers/jwt');
// use JWT auth to secure the api
app.use('/api',jwt());

// API Routes
app.use('/api', require('./server/routes'));
// error handler
app.use(errorHandler);
app.get('*', function(req, res) {	
  res.status(200).sendFile(path.join(__dirname + `${process.env.AGDIST_PATH}/index.html`));
}); 

// start server
app.listen(PORT, () => console.log(`Listening on ${ PORT } -- ${process.env.MONGODB_URI}`))
  